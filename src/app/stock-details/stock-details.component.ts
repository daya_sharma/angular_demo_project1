import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.scss'],
  providers: [DatacallsService]
})
export class StockDetailsComponent implements OnInit {
  // displayedColumns: string[] = ['position', 'name', 'symbol','c1','c2', 'edit', 'delete'];
  displayedColumns: string[] = ['position', 'name', 'symbol', 'edit', 'delete'];


  dataSource;
  posts;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private DatacallsService: DatacallsService, public snackBar: MatSnackBar) { }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {

    this.DatacallsService.view_finsparc_stockname().subscribe(posts => {
      this.posts = posts.result

      console.log('posts data ----> ', this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });


  }

  onDelete(id) {

    console.log("id-=-=-=-=-=->", id)
    var data = {
      "id": id,
      "table_id": "finsparc_stock_id",
      "table": "finsparc_stockname"
    }

    this.DatacallsService.universalDelete(data).subscribe(posts => {
      // this.posts = posts.result;
      this.snackBar.open('Deleted Successfully', '', {
        duration: 2000
      });
      this.ngOnInit();
    });

  }

}
