import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-oi-score',
  templateUrl: './oi-score.component.html',
  styleUrls: ['./oi-score.component.scss'],
  providers:[DatacallsService]
})
export class OiScoreComponent implements OnInit {

  posts;
  fileToupload;

  displayedColumns: string[] = ['position', 'dates','days','days1','days2','days3','edit'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _DatacallsService: DatacallsService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    
    this._DatacallsService.viewfinsparcoiscore().subscribe(posts => {
      this.posts = posts.result
      console.log('posts---=-=asdasd-=-->>',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });

  }

}
