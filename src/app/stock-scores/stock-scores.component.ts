import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'app-stock-scores',
  templateUrl: './stock-scores.component.html',
  styleUrls: ['./stock-scores.component.scss'],
  providers: [DatacallsService, DatePipe]
})
export class StockScoresComponent implements OnInit {
  displayedColumns = ['id', 'invoiceno', 'invoice', 'packing', 'status', 'date', 'amount', 'retailer', 'asm'];
  // , 'logistic', 'docket', 'edit', 'last_update'
  filterForm:FormGroup;
  myDate = new Date();

  constructor(private _fb: FormBuilder, public datacalls: DatacallsService, public snackBar: MatSnackBar, private Router: Router, public dialog: MatDialog, @Inject(DOCUMENT) doc: any, private datePipe: DatePipe) {
    this.filterForm = this._fb.group({

      stockSymbol: [''],
      start_date:['']

    });
    //this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }

  postsSymbol = [];
  posts=[];
  date;
  symbol;
  data;
  dataSource;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {

    // this.date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    // this.symbol = 'ACC';
    this.datacalls.viewFinsparcStockList().subscribe(posts => {
      this.postsSymbol = posts.result;
      console.log('users', posts.result)
    });


  }

  onSubmit() {

this.data=this.filterForm.value.start_date;
this.symbol=this.filterForm.value.stockSymbol;
console.log('date',this.data);
console.log('date',this.symbol);

const momentDate = new Date(this.data); // Replace event.value with your date value
 const formattedDate = moment(momentDate).format("YYYY-MM-DD");
 console.log(formattedDate);

    this.datacalls.viewFinsScores(formattedDate, this.symbol).subscribe(posts => {
      this.posts = posts.data;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log('users', this.posts)
    });
  }





}
