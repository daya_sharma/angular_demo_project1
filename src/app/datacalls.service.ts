import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

@Injectable()
export class DatacallsService {
  // ip = 'http://35.154.49.28:8005';
  ip = 'http://3.6.6.59:7005';     //live server
  // ip = 'http://3.6.6.59:7006';     //development server 
  // ip='http://localhost:8005';

  constructor(private http: Http) { }

  verifyLogin(json) {
    return this.http.post(this.ip + '/get_login', json)
      .map(res => res.json());
  }


  uploadBhavCopyExcel(json) {
    return this.http.post(this.ip + '/uploadBhavCopyExcel', json)
      .map(res => res.json());
  }

  viewFinsparcUser() {
    return this.http.get(this.ip + '/viewFinsparcUserMaster/')
      .map(res => res.json());
  }

  // viewFinsparcMarketPulseAdmin
  viewFinsparcMarketPulseAdmin() {
    return this.http.get(this.ip + '/viewFinsparcMarketPulseAdmin/')
      .map(res => res.json());
  }


  viewFinsparc_score() {
    return this.http.get(this.ip + '/view_finsparc_score/')
      .map(res => res.json());
  }
  viewFinsparc_scorecal() {
    return this.http.get(this.ip + '/view_finsparc_scorecal/')
      .map(res => res.json());
  }

  patch_viewFinsparc_score(id) {
    return this.http.get(this.ip + '/view_finsparc_score/' + id)
      .map(res => res.json());
  }

  patch_viewFinsparc_scorecal(id) {
    return this.http.get(this.ip + '/view_finsparc_scorecal/' + id)
      .map(res => res.json());
  }

  viewFinsparcStockList() {
    return this.http.get(this.ip + '/viewFinsparcStockList/null')
      .map(res => res.json());
  }

  viewFinsparcPortFolio(user_id) {
    return this.http.get(this.ip + '/viewfinsparc_portfolio/null/' + user_id + '/null')
      .map(res => res.json());
  }


  uploadFinsparcLotSize(json) {
    return this.http.post(this.ip + '/uploadFinsparcLotSize', json)
      .map(res => res.json());
  }

  // upload_finsparc_marketpulse
  upload_finsparc_marketpulse(json) {
    return this.http.post(this.ip + '/upload_finsparc_marketpulse', json)
      .map(res => res.json());
  }

  // uploadIVReport
  uploadIVReport(json) {
    return this.http.post(this.ip + '/uploadIVReport/', json)
      .map(res => res.json());
  }


  // upload_finsparc_marketpulse_vix
  upload_finsparc_marketpulse_vix(json) {
    return this.http.post(this.ip + '/upload_finsparc_marketpulse_vix', json)
      .map(res => res.json());
  }
  
  // marketpulse_vix_file_upload
  marketpulse_vix_file_upload(json) {
    return this.http.post(this.ip + '/marketpulse_vix_file_upload', json)
      .map(res => res.json());
  }

  // viewfinsparcmarketpulse
  viewfinsparcmarketpulse() {
    return this.http.get(this.ip + '/viewfinsparcmarketpulse/')
      .map(res => res.json());
  }

    // view_industry_stock
    view_industry_stock(id,parent_id) {
      return this.http.get(this.ip + '/view_industry_stock/'+id+'/'+parent_id)
        .map(res => res.json());
    }

    // view_nifty50_stock
    view_nifty50_stock(id,parent_id) {
      return this.http.get(this.ip + '/view_nifty50_stock/'+id+'/'+parent_id)
        .map(res => res.json());
    }

    // view_bhavcopy_data
    view_bhavcopy_data() {
      return this.http.get(this.ip + '/view_bhavcopy_data/')
        .map(res => res.json());
    }

    // insert_industry_stock
    insert_industry_stock(json) {
      return this.http.post(this.ip + '/insert_industry_stock/',json)
        .map(res => res.json());
    }

    // insert_nifty50_stock
    insert_nifty50_stock(json) {
      return this.http.post(this.ip + '/insert_nifty50_stock/',json)
        .map(res => res.json());
    }

  viewFinsparcStock(user_id, portfolio_id) {
    return this.http.get(this.ip + '/viewFinsparcStock/null/' + user_id + '/' + portfolio_id + '/null/null')
      .map(res => res.json());
  }

  DeleteFinsparcEditorsNotes(json) {
    return this.http.post(this.ip + '/Delete_editor_notes', json)
      .map(res => res.json());
  }

  insert_finsparcsettlement(json) {
    return this.http.post(this.ip + '/insert_finsparcsettlement', json)
      .map(res => res.json());
  }

  // insert_finsparc_lotsize
  insert_finsparc_lotsize(json) {
    return this.http.post(this.ip + '/insert_finsparc_lotsize', json)
      .map(res => res.json());
  }

  viewFinsparcEditorsNotes() {
    return this.http.get(this.ip + '/viewFinsparcEditorsNotes/null/200')
      .map(res => res.json());
  }

  // viewFinsparcIVReport
  viewFinsparcIVReport() {
    return this.http.get(this.ip + '/viewFinsparcIVReport/')
      .map(res => res.json());
  }

  // viewFinsparcCallPutValue
  viewFinsparcCallPutValue() {
    return this.http.get(this.ip + '/viewFinsparcCallPutValue/')
      .map(res => res.json());
  }

  // viewfinsparccallputvaluexceldownload() {
  //   return this.http.get(this.ip+'/viewfinsparccallputvaluexceldownload/')
  //     .map(res => res.json());
  // }

  viewfinsparccallputvaluexceldownload(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/viewfinsparccallputvaluexceldownload/', options)
      .map(res => res.json());
  }

  // score_reports_download
  score_reports_download(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/score_reports_download/', options)
      .map(res => res.json());
  }

  // oi_chnages_reports_download
  oi_chnages_reports_download(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/oi_chnages_reports_download/', options)
      .map(res => res.json());
  }


// download_FNO_score_near
download_FNO_score_near(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/download_FNO_score_near/', options)
      .map(res => res.json());
  } 

  // download_oi_changes_near
  download_oi_changes_near(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/download_oi_changes_near/', options)
      .map(res => res.json());
  } 

  // score_card_download
  score_card_download(): Observable<File> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arrayBuffer');
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

    return this.http.get(this.ip + '/score_card_download/', options)
      .map(res => res.json());
  } 
// significant_changes_download
significant_changes_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/significant_changes_oi_download/', options)
    .map(res => res.json());
} 

// oi_changes_futures_download
oi_changes_futures_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/oi_changes_futures_download/', options)
    .map(res => res.json());
} 

// adding_most_oi_download
adding_most_oi_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/adding_most_oi_download/', options)
    .map(res => res.json());
} 

// shedding_oi_download
shedding_oi_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/shedding_most_oi_download/', options)
    .map(res => res.json());
} 

// iv_report_download
iv_report_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/iv_report_download/', options)
    .map(res => res.json());
}

// high_low_download
high_low_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip + '/high_low_download/', options)
    .map(res => res.json());
}

// high_download
high_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/high_download/', options)
    .map(res => res.json());
}

// high_7_download
high_7_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/high_7_download/', options)
    .map(res => res.json());
}

// high_11_download
high_11_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/high_11_download/', options)
    .map(res => res.json());
}

// low_download
low_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/low_download/', options)
    .map(res => res.json());
}

// low_7_download
low_7_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/low_7_download/', options)
    .map(res => res.json());
}

// low_11_download
low_11_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/low_11_download/', options)
    .map(res => res.json());
}

// future_report_1_download
future_report_1_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/future_report_1_download/', options)
    .map(res => res.json());
}

// future_report_2_download
future_report_2_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/future_report_2_download/', options)
    .map(res => res.json());
}

// option_report_download
option_report_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/option_report_download/', options)
    .map(res => res.json());
}

// bf_score_reports_download
bf_score_reports_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/bf_score_reports_download/', options)
    .map(res => res.json());
}

// bf_score_reports_download_near
bf_score_reports_download_near(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/bf_score_reports_download_near/', options)
    .map(res => res.json());
}

// score_reports_download_next
score_reports_download_next(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/score_reports_download_next/', options)
    .map(res => res.json());
}

// mis_report_download
mis_report_download(): Observable<File> {

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  return this.http.get(this.ip + '/mis_report_download/', options)
    .map(res => res.json());
}

  // view_finsparc_dynamicoptions
  view_finsparc_dynamicoptions() {
    return this.http.get(this.ip + '/view_finsparc_dynamicoptions/')
      .map(res => res.json());
  }

// viewfinsparcstocklist
viewfinsparcstocklist() {
  return this.http.get(this.ip + '/viewfinsparcstocklist/null')
    .map(res => res.json());
} 

// marketinternalsdetails
marketinternalsdetails() {
  return this.http.get(this.ip + '/marketinternalsdetails/')
    .map(res => res.json());
} 

// view_fno_score_near_data
view_fno_score_near_data() {
  return this.http.get(this.ip + '/view_fno_score_near_data/')
    .map(res => res.json());
} 

// viewfinsparcbhavcopy
  viewfinsparcbhavcopy_data(stock,option_type,expiry) {
  // console.log('stock...........................>',stock)
    return this.http.get(this.ip + '/viewfinsparcbhavcopy/null/null/'+stock+'/'+option_type+'/'+expiry)
      .map(res => res.json());
  }

   

    // view_marketpulse_data
    view_marketpulse_data() {
      return this.http.get(this.ip + '/view_marketpulse_data/')
        .map(res => res.json());
    }

  // view_finsparc_stockname
  view_finsparc_stockname() {
    return this.http.get(this.ip + '/view_finsparc_stockname/')
      .map(res => res.json());
  }


  // patch_view_finsparc_dynamicoptions
  patch_view_finsparc_dynamicoptions(id) {
    return this.http.get(this.ip + '/view_finsparc_dynamicoptions/' + id)
      .map(res => res.json());
  }

  // patch_view_finsparc_stockname
  patch_view_finsparc_stockname(id) {
    return this.http.get(this.ip + '/view_finsparc_stockname/' + id)
      .map(res => res.json());
  }



  viewFinsparcEditorsNotes1(id) {
    return this.http.get(this.ip + '/viewFinsparcEditorsNotes/' + id)
      .map(res => res.json());
  }

  insertFinsparcEditorsNotes(json) {
    console.log("api connected ...");
    return this.http.post(this.ip + '/insertFinsparcEditorsNotes', json)
      .map(res => res.json());
  }

  // dashboard_data_count
  dashboard_data_count() {
    return this.http.get(this.ip + '/dashboard_data_count')
      .map(res => res.json());
  }

  // insert_finsparc_dynamicoptions
  insert_finsparc_dynamicoptions(json) {
    return this.http.post(this.ip + '/insert_finsparc_dynamicoptions', json)
      .map(res => res.json());
  }

  // insert_finsparc_stockname
  insert_finsparc_stockname(json) {
    return this.http.post(this.ip + '/insert_finsparc_stockname', json)
      .map(res => res.json());
  }

  // universalDelete
  universalDelete(json) {
    return this.http.post(this.ip + '/universalDelete', json)
      .map(res => res.json());
  }

  // universalDeleteAWS
  universalDeleteAWS(json) {
    return this.http.post(this.ip + '/universalDeleteAWS', json)
      .map(res => res.json());
  }
  
  // delete_nifty50_stock
  delete_nifty50_stock(json) {
    return this.http.post(this.ip + '/delete_nifty50_stock', json)
      .map(res => res.json());
  }

  // universalDeleteForLocalAndAWS1
  universalDeleteForLocalAndAWS1(json) {
    return this.http.post(this.ip + '/universalDeleteForLocalAndAWS1', json)
      .map(res => res.json());
  }

  // insert_finsparc_score
  insert_finsparc_score(json) {
    return this.http.post(this.ip + '/insert_finsparc_score', json)
      .map(res => res.json());
  }

  // insert_finsparc_scorecal
  insert_finsparc_scorecal(json) {
    return this.http.post(this.ip + '/insert_finsparc_scorecal', json)
      .map(res => res.json());
  }

  // insert_marketinternals
  insert_marketinternals(json) {
    return this.http.post(this.ip + '/insert_marketinternals', json)
      .map(res => res.json());
  }

  // update_insert_marketinternals
  update_marketinternals(json) {
    return this.http.post(this.ip + '/update_marketinternals', json)
      .map(res => res.json());
  }

  viewFinsScores(date, symbol) {
    return this.http.get(this.ip + '/viewScores/' + date + '/' + symbol)
      .map(res => res.json());
  }

  // view_finsparc_lotchange
  view_finsparc_lotchange(id) {
    return this.http.get(this.ip + '/view_finsparc_lotchange/' + id)
      .map(res => res.json());
  }

  // Update_finsparc_lotchange
  Update_finsparc_lotchange(json) {
    return this.http.post(this.ip + '/Update_finsparc_lotchange/', json)
      .map(res => res.json());
  }



  viewFinsparcScores(date, symbol) {
    return this.http.get(this.ip + '/viewFinsparcScores/' + date + '/' + symbol)
      .map(res => res.json());
  }


  viewfinsparclotsize(page_no, page_size) {
    return this.http.get(this.ip + '/viewfinsparclotsize/' + page_no + '/' + page_size)
      .map(res => res.json());
  }

  patchviewfinsparclotsize(id) {
    return this.http.get(this.ip + '/view_finsparc_lotsize/' + id)
      .map(res => res.json());
  }

  viewfinsparcbhavcopy(page_no, page_size) {
    return this.http.get(this.ip + '/viewfinsparcbhavcopy/' + page_no + '/' + page_size)
      .map(res => res.json());
  }

  uploadFinsparcSettlementDates(json) {
    return this.http.post(this.ip + '/uploadFinsparcSettlementDates', json)
      .map(res => res.json());
  }

  uploadFinsparcHoildays(json) {
    return this.http.post(this.ip + '/uploadFinsparcHoildays', json)
      .map(res => res.json());
  }

  viewfinsparchoilday() {
    return this.http.get(this.ip + '/viewfinsparchoilday')
      .map(res => res.json());
  }

  viewfinsparcsettlement() {
    return this.http.get(this.ip + '/viewfinsparcsettlement')
      .map(res => res.json());
  }

  viewfinsparcoiscore() {
    return this.http.get(this.ip + '/viewfinsparcoiscore/null/null')
      .map(res => res.json());
  }


  view_marketinternals(id, cat_id, subcat_id) {
    return this.http.get(this.ip + '/view_marketinternals/' + id + '/' + cat_id + '/' + subcat_id)
      .map(res => res.json());
  }

  viewfinsparcsettlement1(id) {
    return this.http.get(this.ip + '/viewfinsparcsettlement/' + id)
      .map(res => res.json());
  }


  runSchedular() {
    // return this.http.get('http://122.170.5.219:8005/scheduleTime')
    return this.http.get('http://13.235.226.162:8005/scheduleTime')
    // return this.http.get('http://13.235.226.162:8005/scheduleTime')
    .map(res => res.json());
  }
  
  // runScript
  runScript(st_date) {
    console.log(' data--->>',st_date)
    return this.http.get(this.ip + '/runScript/'+st_date)
      .map(res => res.json());
  }

  // scheduler_info
  scheduler_info() {
    return this.http.get(this.ip + '/scheduler_info')
      .map(res => res.json());
  }
}
