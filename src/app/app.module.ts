import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CustomOverlayContainer } from './theme/utils/custom-overlay-container';

import { AgmCoreModule } from '@agm/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: true,
  suppressScrollX: true               
};
import { CalendarModule } from 'angular-calendar';
import { SharedModule } from './shared/shared.module';
import { PipesModule } from './theme/pipes/pipes.module';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { BlankComponent } from './pages/blank/blank.component';
import { SearchComponent } from './pages/search/search.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { ErrorComponent } from './pages/errors/error/error.component';
import { AppSettings } from './app.settings';

import { SidenavComponent } from './theme/components/sidenav/sidenav.component';
import { VerticalMenuComponent } from './theme/components/menu/vertical-menu/vertical-menu.component';
import { HorizontalMenuComponent } from './theme/components/menu/horizontal-menu/horizontal-menu.component';
import { BreadcrumbComponent } from './theme/components/breadcrumb/breadcrumb.component';
import { FlagsMenuComponent } from './theme/components/flags-menu/flags-menu.component';
import { FullScreenComponent } from './theme/components/fullscreen/fullscreen.component';
import { ApplicationsComponent } from './theme/components/applications/applications.component';
import { MessagesComponent } from './theme/components/messages/messages.component';
import { UserMenuComponent } from './theme/components/user-menu/user-menu.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { InportfolioComponent } from './pages/inportfolio/inportfolio.component';
import { BhavcopyComponent } from './pages/bhavcopy/bhavcopy.component';
import { LotsizeComponent } from './pages/lotsize/lotsize.component';
import { HoildaysComponent } from './pages/hoildays/hoildays.component';
import { SettlementComponent } from './pages/settlement/settlement.component';
import { StockScoresComponent } from './stock-scores/stock-scores.component';
import { OptionScoresComponent } from './option-scores/option-scores.component';
import { EditorNotesComponent,NoteDialog } from './editor-notes/editor-notes.component';
import { AddnotesComponent } from './addnotes/addnotes.component';

import { NewSharedModule } from '../common/shared.module';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { GridAllModule } from '@syncfusion/ej2-angular-grids';
 

// httpModule use to featch the data from data calls 
import { HttpModule, JsonpModule } from '@angular/http';
import { HtmlEditorComponent } from './pages/html-editor/html-editor.component';
import { AuthguardGuard } from './authguard.guard';
import { AddScoreComponent } from './pages/add-score/add-score.component';
// import { MarketPulseComponent } from './pages/market-pulse/market-pulse.component';
import { MytableComponent } from './pages/mytable/mytable.component';
import { AddLotsizeComponent } from './pages/add-lotsize/add-lotsize.component';
import { IvrReportComponent } from './pages/ivr-report/ivr-report.component';
import { CallPutValuesComponent } from './pages/call-put-values/call-put-values.component';
import { DynamicOptionsComponent } from './pages/dynamic-options/dynamic-options.component';
import { AddDynamicOptionsComponent } from './pages/add-dynamic-options/add-dynamic-options.component';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { AddStockDetailsComponent } from './add-stock-details/add-stock-details.component';
import { AddSettlementComponent } from './pages/add-settlement/add-settlement.component';
import { MarketinternalComponent } from './pages/marketinternal/marketinternal.component';
import { AddMarketinternalComponent } from './pages/add-marketinternal/add-marketinternal.component';
import { MarketinternalCatComponent } from './pages/marketinternal-cat/marketinternal-cat.component';
import { MarketinternalSubcatComponent } from './pages/marketinternal-subcat/marketinternal-subcat.component';
import { OiScoreComponent } from './oi-score/oi-score.component';
import { DataCurrentBhavcopyComponent } from './pages/data-current-bhavcopy/data-current-bhavcopy.component';
import { DataMarketPulseComponent } from './pages/data-market-pulse/data-market-pulse.component';
import { AddLotChangeComponent } from './pages/add-lot-change/add-lot-change.component';

import { IndustryStockComponent } from './pages/industry-stock/industry-stock.component';
import { AddIndustryStockComponent } from './pages/add-industry-stock/add-industry-stock.component';
// import { Nifty50StockComponent } from './pages/nifty50-stock/nifty50-stock.component';
import { AddNifty50StockComponent } from './pages/add-nifty50-stock/add-nifty50-stock.component';
import { DataMasterComponent } from './pages/data-master/data-master.component';
import { DataMarketInternalsComponent } from './pages/data-market-internals/data-market-internals.component';

import { AddTradeComponent } from './pages/add-trade/add-trade.component';
import { AddIndustryComponent } from './pages/add-industry/add-industry.component';
import { JobsComponent } from './pages/jobs/jobs.component';
import { UploadsComponent } from './pages/uploads/uploads.component';
// import { ScoreDetailsComponent } from './pages/score-details/score-details.component';
import { AddScoreDetailsComponent } from './pages/add-score-details/add-score-details.component';
// import { DownloadsComponent } from './pages/downloads/downloads.component';
// import { TradeComponent } from './pages/trade/trade.component';
// import { LotChangeComponent } from './pages/lot-change/lot-change.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,     
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBNcjxo_35qnEG17dQvvftWa68eZWepYE0'
    }), 
    PerfectScrollbarModule, 
    CalendarModule.forRoot(),
    SharedModule,
    PipesModule,
    routing,
    
    NewSharedModule,RichTextEditorAllModule,CheckBoxModule, DialogModule, NumericTextBoxModule, HttpModule, JsonpModule,GridAllModule
  ],
  declarations: [
    AppComponent,
    PagesComponent,
    BlankComponent,
    SearchComponent,
    NotFoundComponent,
    ErrorComponent,
    SidenavComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    BreadcrumbComponent,
    FlagsMenuComponent,
    FullScreenComponent,
    ApplicationsComponent,
    MessagesComponent,
    UserMenuComponent,
    PortfolioComponent,
    InportfolioComponent,
    BhavcopyComponent,
    LotsizeComponent,
    HoildaysComponent,
    SettlementComponent,
    MarketinternalComponent,
    StockScoresComponent,
    OptionScoresComponent,
    EditorNotesComponent,
    NoteDialog,
    AddnotesComponent,
    HtmlEditorComponent,
    AddScoreComponent,
    // MarketPulseComponent,
    MytableComponent,
    AddLotsizeComponent,
    IvrReportComponent,
    CallPutValuesComponent,
    DynamicOptionsComponent,
    AddDynamicOptionsComponent,
    StockDetailsComponent,
    AddStockDetailsComponent,
    AddSettlementComponent,
    AddMarketinternalComponent,
    MarketinternalCatComponent,
    MarketinternalSubcatComponent,
    OiScoreComponent,
    DataCurrentBhavcopyComponent,
    DataMarketPulseComponent,
    AddLotChangeComponent,

    IndustryStockComponent,

    AddIndustryStockComponent,

    // Nifty50StockComponent,

    AddNifty50StockComponent,

    DataMasterComponent,

    DataMarketInternalsComponent,

    AddTradeComponent,

    AddIndustryComponent,

    JobsComponent,

    UploadsComponent,

    // ScoreDetailsComponent,

    AddScoreDetailsComponent,

    


    // DownloadsComponent,

    // TradeComponent,

  ],
  entryComponents:[
    VerticalMenuComponent,NoteDialog
  ],
  providers: [ AuthguardGuard,
    AppSettings,
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    { provide: OverlayContainer, useClass: CustomOverlayContainer }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }