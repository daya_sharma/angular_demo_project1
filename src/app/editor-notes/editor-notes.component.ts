import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';

import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-editor-notes',
  templateUrl: './editor-notes.component.html',
  styleUrls: ['./editor-notes.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  providers: [DatacallsService, DatePipe]
})
export class EditorNotesComponent implements OnInit {
  public modeselect = 'daily';
  // selected='option1';
  onChange(val:string){
    this.modeselect=val;
    console.log('value----------------=>',val);

  }

  columnsToDisplay = ['finsparc_editors_notes_id','type','cdate'];
  // displayedColumns = ['id', 'description','created_on','delete_note','edit'];
  // , 'logistic', 'docket', 'edit', 'last_update'
  // filterForm:FormGroup;
  myDate = new Date();

  constructor(private _fb: FormBuilder, public datacalls: DatacallsService, public snackBar: MatSnackBar, private Router: Router, public dialog: MatDialog, @Inject(DOCUMENT) doc: any, private datePipe: DatePipe) {
    // this.filterForm = this._fb.group({

    //   stockSymbol: [''],
    //   start_date:['']

    // });
    //this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }

  postsSymbol = [];
  posts=[];
  date;
  symbol;
  data;
  dataSource;
  // public shipFormat: Object;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {

    this.datacalls.viewFinsparcEditorsNotes().subscribe(posts => {
      this.posts = posts.result;
      console.log('users', posts.result)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  openDialog(id,description,lengths): void {
    console.log("note id ---=-=-=>",id)
    const dialogRef = this.dialog.open(NoteDialog, {
      // width: '250px',
      // data: mydata
      data: {
        "id": id,
        "description":description,
        "lenght":lengths
      } 
    });
    // debugger
    dialogRef.afterClosed().subscribe(result => {
      console.log('The note dialog was closed');

    });
  }

  onDelete(id){

        console.log("id-=-=-=-=-=->",id)
        var data={
          "id":id
        }

        this.datacalls.DeleteFinsparcEditorsNotes(data).subscribe(posts => {
          // this.posts = posts.result;
          this.snackBar.open('Notes Deleted Successfully', '', {
            duration: 2000
          });
          this.ngOnInit();
        });
        
    }

}



/***************************Start Note Dialog********************************************/

@Component({
  selector: 'note-dialog',
  templateUrl: 'note-dialog.html',
  // styleUrls: [''],
  providers: [DatacallsService]
})
export class NoteDialog implements OnInit {
  posts;
  // data1;
  description;
  abc;
  daya;


  constructor(
    public dialogRef: MatDialogRef<NoteDialog>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData, private _DatacallsService: DatacallsService) {
    console.log('dialog data----->', data)
  }

  ngOnInit() {
    // let data1 = {
    //   des: "this.data.description"
    // }
  

    // const myObjStr = JSON.stringify(data1);
    // const myObjStr = parseInt(this.data.description);
    
       this.daya = this.data.description;

    //  .slice(0,this.abc.lenght);

    console.log('daya',this.daya);

    // console.log('dia', this.abc.description.slice(0,this.abc.lenght))
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export interface DialogData {
  description: string;
}