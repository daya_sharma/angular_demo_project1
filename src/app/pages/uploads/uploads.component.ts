import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar } from '@angular/material';
import { empty } from 'rxjs';

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.scss'],
  providers: [DatacallsService]
})
export class UploadsComponent implements OnInit {
  posts;
  post;
  fileToupload;
  fileToupload1;
  fileToupload2;
  fileToupload3;
  fileToupload4;
  cdate;

  tot_bhav;
  prev_bhav;
  tot_marketpulse;
  tot_ivreport;
  prev_ivreport;
  tot_lots;
  uploaded_bhavcopy;
  uploaded_marketpulse_vix;
  uploaded_marketpulse;
  uploaded_iv_report;

  constructor(private DatacallsService: DatacallsService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    
    console.log('ngOnInit()------>>>>');

    this.DatacallsService.dashboard_data_count().subscribe(posts=>{
    this.post=posts.result[0]; 
      console.log('post data -=-=0=-=>>',this.post)
     this.tot_bhav=posts.result[0].totalbhavcopy;
     this.prev_bhav=posts.result[0].previous_bhavcopy_datacount;
     this.tot_marketpulse=posts.result[0].total_marketpulse_data;
     this.tot_ivreport=posts.result[0].iv_report_tot;
     this.prev_ivreport=posts.result[0].previous_day_ivreport;
    //  this.tot_lots=posts.results[0].tot_lots;
    this.uploaded_bhavcopy=posts.result[0].uploaded_bhavcopy; 
    this.uploaded_marketpulse_vix=posts.result[0].uploaded_marketpulse_vix; 
    this.uploaded_marketpulse=posts.result[0].uploaded_marketpulse; 
    this.uploaded_iv_report=posts.result[0].uploaded_iv_report; 

    })

  }
  //Marketpulse Fii
  onChange(fileInput: any) {
    this.fileToupload = fileInput.target.files['0']
  }
  //MarketPulse VIX
  onChange1(fileInput1: any) {
    this.fileToupload1 = fileInput1.target.files['0']
  }
  //bhavcopy
  onChange2(fileInput2: any) {
    this.fileToupload2 = fileInput2.target.files['0']
  }
  //IV Report
  onChange3(fileInput3: any) {
    this.fileToupload3 = fileInput3.target.files['0']
  }
  //Lot Size
  onChange4(fileInput4: any) {
    this.fileToupload4 = fileInput4.target.files['0']
  }
  /****
   * Bhavcopy Upload function
   * ***/
  onSubmitBhavCopy() {

    var formData2 = new FormData();

    this.snackBar.open('file Upload Succeeded Please Wait.........', '', {
      duration: 2000,
      announcementMessage: "started",
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });

    formData2.append('file1', this.fileToupload2);
    this.DatacallsService.uploadBhavCopyExcel(formData2).subscribe(posts => {
      console.log('Bhavcopy Posts------->>>>', posts)
      if (posts.result[0].status == 201) {
        this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 4000
        });
        this.ngOnInit();
      }
      else if (posts.result[0].status == 404) {
        this.snackBar.open('Please Choose File First', '', {
          duration: 2000
        });
      }
      else if (posts.result[0].status == 203) {
        this.snackBar.open('Column Name Incorrect or wrong file choosen', '', {
          duration: 2000
        });
      }
      else if (posts.status == 202) {
        this.snackBar.open('OPEN_INT Should be greater than or equal to zero', '', {
          duration: 2000
        });
      }
      else {
        this.snackBar.open('Excel Uploded Successfully', '', {
          duration: 8000
        });
        // this.ngOnInit();
        window.location.reload();
      }
    });

    // this.snackBar.open('Completed..', '', {
    //   duration: 200
    // });
    this.ngOnInit();
  }

  /*****
   * Marketpulse Fii Data
   * ******/
  onSubmit() {
    /************
     * select id,index_future,index_option,stock_future,stock_option,cdate from finsparc_marketpulse where cdate::date='2019-10-15'
     * ***************/
    var formData = new FormData();

    formData.append('file1', this.fileToupload);

    this.DatacallsService.upload_finsparc_marketpulse(formData).subscribe(posts => {
      console.log('posts--->', posts.result[0].status)
      if (posts.result[0].status == 204) {
        this.snackBar.open('Please Upload Market Pulse VIX File First.', '', {
          duration: 2000
        });
        this.ngOnInit();
      }
      else if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploaded Successfully', '', {
          duration: 2000
        });
        // this.ngOnInit();
        window.location.reload();
      }
      else if (posts.result[0].status == 203) {
        this.snackBar.open('Your Column Name incorrect.', '', {
          duration: 2000
        });
      }
      else if (posts.result[0].status == 404) {
        this.snackBar.open('Please Choose File', '', {
          duration: 2000
        });
      }
      else {

        this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 2000
        });
      }
    });


  }

  /*****
   * MarketPulse Vix Data
   * ******/
  onSubmit1() {
    /*************
     *  select id,open,high,low,close,prev_close,change,per_change,cdate from finsparc_marketpulse where cdate::date='2019-10-15'
     * ************/
    var formData = new FormData();

    formData.append('file1', this.fileToupload1);

    console.log('Form Data -=-=->>', formData)
    this.DatacallsService.upload_finsparc_marketpulse_vix(formData).subscribe(posts => {

      if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploaded Successfully', '', {
          duration: 2000
        });

        // this.ngOnInit();
        window.location.reload();
      }
      else if (posts.result[0].status == 203) {
        this.snackBar.open('Column Name Incorrect', '', {
          duration: 2000
        });
      }
      else if (posts.result[0].status == 404) {
        this.snackBar.open('Please Choose File', '', {
          duration: 2000
        });
      }
      else {
        this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 2000
        });
      }
    });
  }
  /*******
   * IV Report
   * ********/
  onSubmitIVReport() {

    var formData3 = new FormData();

    formData3.append('file1', this.fileToupload3);
    this.DatacallsService.uploadIVReport(formData3).subscribe(posts => {

      if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploded Successfully', '', {
          duration: 2000
        });
        // this.ngOnInit();
        window.location.reload();
      }
      else if (posts.result[0].status == 203) {
        this.snackBar.open('Column Name Incorrect', '', {
          duration: 2000
        });
      }
      else if (posts.result[0].status == 404) {
        this.snackBar.open('Please Choose File First', '', {
          duration: 2000
        });
      }
      else {

        this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 3000
        });
      }
    });


  }

  /*******
   * LotSize Data uploaded
   * *******/
  onSubmitLotSize() {

    var formData4 = new FormData();

    formData4.append('file1', this.fileToupload4);
    this.snackBar.open('Uploading..........  ', '', {
      duration: 2000
    });
    this.DatacallsService.uploadFinsparcLotSize(formData4).subscribe(posts => {
      console.log('data------>', posts)
      this.snackBar.open('Excel File is Uploading..........  ', '', {
        duration: 2000
      });

      if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploaded Successfully', '', {
          duration: 2000
        });
        // this.ngOnInit();
        window.location.reload();
      }
      else if (posts.result[0].status == 203) {
        this.snackBar.open('Column Name Incorrect', '', {
          duration: 2000
        });
      }
      else if (posts.result[0].status == 404) {
        this.snackBar.open('Please Choose File First', '', {
          duration: 2000
        });
      }
      else {
        this.snackBar.open('Please Try Again', '', {
          duration: 2000
        });
      }
    });

  }


}
