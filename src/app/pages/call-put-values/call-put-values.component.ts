import { Component, OnInit ,ViewChild} from '@angular/core';
import { DatacallsService } from '../../datacalls.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-call-put-values',
  templateUrl: './call-put-values.component.html',
  styleUrls: ['./call-put-values.component.scss'],
  providers:[DatacallsService]
})
export class CallPutValuesComponent implements OnInit {
  

  displayedColumns: string[] = ['position', 'name', 'symbol', 'date'];
  dataSource;
  posts;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private DatacallsService:DatacallsService,public snackBar:MatSnackBar) { }

  ngOnInit() {

    this.DatacallsService.viewFinsparcCallPutValue().subscribe(posts => {
      this.posts = posts.result

      console.log('posts data ----> ',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });

  }


  download(){
    console.log('inside')
    this.DatacallsService.viewfinsparccallputvaluexceldownload().subscribe(data => {
      // this.posts = posts.result;
      var link=document.createElement('a');
        link.href=window.URL.createObjectURL(data);
        link.download="callputvalue.xlsx";
        link.click();

    }); 
  }
}
