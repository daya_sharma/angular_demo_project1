import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-nifty50-stock',
  templateUrl: './nifty50-stock.component.html',
  styleUrls: ['./nifty50-stock.component.scss'],
  providers:[DatacallsService]
})
export class Nifty50StockComponent implements OnInit {

  displayedColumns: string[] = ['id','name','edit','delete'];
  dataSource;
  posts;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 

  constructor(private DatacallsService:DatacallsService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.DatacallsService.view_nifty50_stock(null,0).subscribe(post=>{
      this.posts=post.result;
      console.log('posts data-=-=->>',this.posts);

      this.dataSource=new MatTableDataSource(this.posts);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

    })


  }

  onDelete(id){

    console.log("id-=-=-=-=-=->",id)
    var data={
      "id":id
    }
    console.log('data++++++>>',data)
    this.DatacallsService.delete_nifty50_stock(data).subscribe(posts => {
      // this.posts = posts.result;
      this.snackBar.open('Data Deleted Successfully', '', {
        duration: 2000
      });
      this.ngOnInit(); 
    });
    
}

}
