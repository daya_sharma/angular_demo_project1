import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Nifty50StockComponent } from './nifty50-stock.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes = [
  { path: '', component: Nifty50StockComponent, pathMatch: 'full' }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes), 
    SharedModule
  ],
  declarations: [Nifty50StockComponent],
  providers:[]
})
export class Nifty50StockModule { }
