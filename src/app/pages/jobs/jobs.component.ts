import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

export interface Min {
  value: number;
  viewValue: string;
}
export interface Hr {
  value: number;
  viewValue: string;
}
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
  providers:[DatacallsService]
})
export class JobsComponent implements OnInit {
FormData;
post;
st_date;
post1;
status;
fileToupload1;
constructor(private _DatacallsService:DatacallsService,private ActivatedRoute:ActivatedRoute,private FormBuilder:FormBuilder,public snackBar:MatSnackBar,private router:Router) {
  this.FormData = this.FormBuilder.group({
    // id: [0],
    from_date: [''],
    // to_date:[''],
    // from_time: [''],
    // to_time:[''],
    // active_status:[1],
    // pdf_name:['', Validators.required]
  })

   }

  ngOnInit() {
    // scheduler_info
    this._DatacallsService.scheduler_info().subscribe(post => {
      this.post1=post.result;
      this.status=post.result[0].status;
   console.log('post1----------->>',this.post1)
   console.log('status----------->>',this.status)
    });      
  }

  //MarketPulse VIX
  onChange1(fileInput1: any) {
    this.fileToupload1 = fileInput1.target.files['0']
  }

    /*****
   * MarketPulse Vix Data
   * ******/
  onSubmitVIX() {
    /*************
     *  select id,open,high,low,close,prev_close,change,per_change,cdate from finsparc_marketpulse where cdate::date='2019-10-15'
     * ************/
    var formData = new FormData();

    formData.append('file1', this.fileToupload1);

    console.log('Form Data -=-=->>', formData)
    this._DatacallsService.marketpulse_vix_file_upload(formData).subscribe(posts => {
      this.post1=posts.result;
      console.log('posts1 --status---->>>>',this.post1.status)
      if (posts.result[0].status == 200) {
        this.snackBar.open('Your File is Uploaded Successfully...', '', {
          duration: 4000
        });
                this.ngOnInit();
                // window.location.reload();
      }
      else if (posts.result[0].status == 403) {
        this.snackBar.open('Please Choose File', '', {
          duration: 2000
        });
      }
      else {
        this.snackBar.open('Something went wrong', '', {
          duration: 4000
        });
      }
    });
  }

  onSubmit(){
    console.log('inside submit')
    this.st_date=this.FormData.value.from_date
        
        console.log('data------>> ',this.st_date);
      this.snackBar.open('Scheduler going to start.... ', '', {
        duration: 2000
      });

    this._DatacallsService.runScript(this.st_date).subscribe(posts => {
     this.post=posts.result;
      console.log('post data ',this.post)
    });      

  }

  runScheduler(){
    this._DatacallsService.runSchedular().subscribe(posts => {
      console.log('data------>',posts)
      if (posts['status'] == 200) {
        this.snackBar.open('Schedular Will Start In 10 Min ', '', {
          duration: 4000
        });
        
      }
     else if (posts['status'] == 401) {
        this.snackBar.open('Please add the Adjustment factor in corporate action(Lot Change tab) ', '', {
          duration: 4000
        });
        
      }
      else{
        this.snackBar.open('Some Files are missing(Not uploaded)', '', {
          duration: 4000
        });
      }

    });

  }


}
