import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-marketinternal-subcat',
  templateUrl: './marketinternal-subcat.component.html',
  styleUrls: ['./marketinternal-subcat.component.scss'],
  providers: [DatacallsService]
})
export class MarketinternalSubcatComponent implements OnInit {
  posts;
  displayedColumns: string[] = ['id', 'name', 'edit'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  id;
  subcat_id;
cat_id;
  constructor(private Datacallservice: DatacallsService, public snackBar: MatSnackBar, private ActivatedRoute: ActivatedRoute, private router: Router, private FormBuilder: FormBuilder) { }

  ngOnInit() {

    this.cat_id = this.ActivatedRoute.snapshot.params['cat_id'];
    this.subcat_id = this.ActivatedRoute.snapshot.params['id'];
    console.log('cat_id---=-=-=-->>', this.cat_id)
    console.log('subcat_id---=-=-=-->>', this.subcat_id)
    this.Datacallservice.view_marketinternals(null, this.cat_id,this.subcat_id).subscribe(posts => {
      this.posts = posts.result
      console.log('posts---=-=-=-->>', this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });

  }

}
