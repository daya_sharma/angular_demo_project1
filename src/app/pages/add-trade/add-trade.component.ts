import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';



@Component({
  selector: 'app-add-trade',
  templateUrl: './add-trade.component.html',
  styleUrls: ['./add-trade.component.scss'],
  providers: [DatacallsService]
})
export class AddTradeComponent implements OnInit {
  tradeForm;
  post;
  posts;
  posts1;
  posts2;
  id;
  stock;
  stk_option;
  expiry1;
  series=[];
  num1;
  num2;
  lot3;


  public modeselect = 'near';
  disableSelect = new FormControl(true);


  constructor(private DatacallsService: DatacallsService, private FormBuilder: FormBuilder, public snackBar: MatSnackBar, private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.tradeForm = this.FormBuilder.group({
      id: [0],
      // instrument: ['', Validators.required],
      symbol: ['', Validators.required],
      options: [''],
      instrument_type: ['', Validators.required],
      contracts: ['', Validators.required],
      lot_date: ['', Validators.required],
      series: [''],
      strike_pr:[''],
      lot1: ['', Validators.required],
      lot2: ['', Validators.required],
      lot3: ['', Validators.required],
      price: ['', Validators.required],    
    })
  }

  myControl = new FormControl();
  options: string[] = [];

  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.id = this.ActivatedRoute.snapshot.params['finsparc_lotsize_id'];
    console.log("patch value id -->", this.id);

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.DatacallsService.viewfinsparcstocklist().subscribe(post => {
      this.posts = post.result;
      // this.options.push(this.posts.stock);
      // for (let i = 1; i<=this.posts[i].finsparc_stock_id; i++) {
        for (let i = 0; i<this.posts.length; i++) {
        this.options.push(this.posts[i].stock);
        console.log('len....',this.posts.length);
        console.log('push data ____________>>>', this.options.push(this.posts[i].stock))
      }
    
      console.log('posts data ____________>>>', this.posts)
      
    })


  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


  onChange1(option) {
    
    // console.log('selected option----->', option);
    this.stock=option;
    console.log('selected symbol (stock)----->', this.stock);
    let option_type1='XX';
    let expiry1='near'
    this.DatacallsService.viewfinsparcbhavcopy_data(option,option_type1,expiry1).subscribe(post => {

      this.posts1 = post.result;
      console.log('Posts1 data---->>', this.posts1);
      
      
      this.tradeForm.patchValue({
        // id: this.posts1[0].id,
        instrument_type: this.posts1[0].instrument,
        contracts:this.tradeForm.value.contracts,
        series:this.posts1[0].expiry_dt, 
        strike_pr:this.posts1[0].strike_pr,
        lot2:this.posts1[0].stock,
        price:this.posts1[0].close,
        lot_date:this.posts1[0].cdate
      })
    
    })

  }
  

  public onChange2(val) {  // event will give you full breif of action
    this.stk_option=val;
    console.log('onChange2 Selected value--> ',this.stk_option);
    console.log('posts1 Selected value--> ', this.stock);
    let expiry1 = 'near';

    this.DatacallsService.viewfinsparcbhavcopy_data(this.stock, this.stk_option, expiry1).subscribe(post => {
      this.posts1 = post.result;
      console.log('posts2 data -=-=-=-=>  ', this.posts1);

      this.tradeForm.patchValue({
        // id: this.posts1[0].id,
        options:this.posts1[0].symbol,
        // instrument_type: this.posts1[0].instrument,
        contracts: this.tradeForm.value.contracts,
        series: this.posts1[0].expiry_dt,
        strike_pr: this.posts1[0].strike_pr,
        lot2: this.posts1[0].stock,
        price: this.posts1[0].close,
        lot_date: this.posts1[0].cdate
      })


    })


  }

  public onChange3(val) {  // event will give you full breif of action
    this.expiry1=val;
    console.log('onChange3 expiry1 Selected value--> ',this.expiry1);
    console.log('onChange3 stk_price Selected value--> ',this.stk_option);
    console.log('posts1 Selected value--> ',this.stock);
    let expiry1='near';


this.DatacallsService.viewfinsparcbhavcopy_data(this.stock,this.stk_option,this.expiry1).subscribe(post=>{
this.posts1=post.result;
console.log('posts2 data -=-=-=-=>  ',this.posts1);

this.tradeForm.patchValue({
  // id: this.posts1[0].id,
  options:this.posts1[0].symbol,
  // instrument_type: this.posts1[0].instrument,
  contracts:this.tradeForm.value.contracts,
  series:this.posts1[0].expiry_dt,
  strike_pr:this.posts1[0].strike_pr,
  lot2:this.posts1[0].stock,
  price:this.posts1[0].close,
  lot_date:this.posts1[0].cdate
})


})


 }

 onSearchChange(event:number){

  // let num2:number
  this.num1=event;
  this.num2=(this.posts1[0].stock);
  this.lot3=this.num1*this.num2;

  console.log('value1-=-=',this.num1);
  console.log('value2-=-=',this.num2);
  console.log('ans -=-=',this.lot3);

  this.tradeForm.patchValue({
    lot3:this.lot3
  })

}



}
