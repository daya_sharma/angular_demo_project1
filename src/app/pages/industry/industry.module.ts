import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndustryComponent } from './industry.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes=[
  { path:'',component:IndustryComponent,pathMatch:'full'}
]

@NgModule({ 
  imports: [  
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [IndustryComponent],
  providers:[] 
})
export class IndustryModule { }
