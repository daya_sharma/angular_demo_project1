import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-industry',
  templateUrl: './industry.component.html',
  styleUrls: ['./industry.component.scss'],
  providers:[DatacallsService]
})
export class IndustryComponent implements OnInit {

  displayedColumns: string[] = ['id','name','sub1','edit','delete'];
  dataSource;
  posts;
  
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 

  constructor(private DatacallsService:DatacallsService,private snackBar:MatSnackBar) { }

  ngOnInit() {

    this.DatacallsService.view_industry_stock(null,0).subscribe(post=>{
      this.posts=post.result;
      console.log('posts data-=-=->>',this.posts);

      this.dataSource=new MatTableDataSource(this.posts);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

    })
  }

  onDelete(id){

    console.log("id-=-=-=-=-=->",id)
    var data={
      "id":id
    }
    console.log('data++++++>>',data)
    this.DatacallsService.universalDeleteAWS(data).subscribe(posts => {
      // this.posts = posts.result;
      this.snackBar.open('Data Deleted Successfully', '', {
        duration: 2000
      });
      this.ngOnInit();
    });
    
}


}
