import { Component, OnInit,ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import {DatacallsService} from '../../datacalls.service';
import { ActivatedRoute } from '@angular/router';


// export interface PeriodicElement1 {
// relised:number;
// unrelised:number;
// total:number;
// }
// const ELEMENT_DATA1: PeriodicElement[] = [
//   { relised:1500, unrelised:1217, total:2850 }
// ]

@Component({
  selector: 'app-inportfolio',
  templateUrl: './inportfolio.component.html',
  styleUrls: ['./inportfolio.component.scss'],
  providers: [ DatacallsService ]  
})
export class InportfolioComponent implements OnInit {
  dataSource;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public displayedColumns = ['position', 'name', 'weight', 'buyQ', 'sellQ', 'netQ','currPrice', 'basePrice'];
  // public dataSource: any;
    
 

  constructor(public datacalls:DatacallsService,private activatedRoute: ActivatedRoute) { }
  user_id;
  portfolio_id
  posts;
  ngOnInit() {

    this.user_id=this.activatedRoute.snapshot.params['id'];
    this.portfolio_id=this.activatedRoute.snapshot.params['portfolio_id']
    console.log('user_id',this.user_id, this.portfolio_id)


    this.datacalls.viewFinsparcStock(this.user_id,this.portfolio_id).subscribe(posts=>{
      this.posts=posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator;
      console.log('users',posts.result)
      });  
  }

}
