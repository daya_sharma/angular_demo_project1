import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar,MatTableDataSource,MatPaginator,MatSort} from '@angular/material';

@Component({
  selector: 'app-marketinternal',
  templateUrl: './marketinternal.component.html',
  styleUrls: ['./marketinternal.component.scss'],
  providers:[DatacallsService]
})
export class MarketinternalComponent implements OnInit {

  posts;
  fileToupload;
  
  displayedColumns: string[] = ['id','name','edit','view'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Datacallservice:DatacallsService,public snackBar:MatSnackBar) { }

  ngOnInit() {
    
    this.Datacallservice.view_marketinternals(null,0,0).subscribe(posts => {
      this.posts = posts.result
      console.log('posts---=-=-=-->>',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });


  }

}
