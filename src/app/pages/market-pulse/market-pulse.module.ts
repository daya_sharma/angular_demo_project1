import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketPulseComponent } from './market-pulse.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


export const routes=[
  { path:'',component:MarketPulseComponent,pathMatch:'full'}
]; 

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [MarketPulseComponent],
  providers: [

  ]
}) 
export class MarketPulseModule { }
