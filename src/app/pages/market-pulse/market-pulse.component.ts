import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from '../../datacalls.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
// import {ScrollDispatchModule} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-market-pulse',
  templateUrl: './market-pulse.component.html',
  styleUrls: ['./market-pulse.component.scss'],
  providers:[DatacallsService]
})
export class MarketPulseComponent implements OnInit {

  // displayedColumns: string[] = ['c1','c2','c3','c4','c5','c6','c7','c8','c3','c10','c11','c12','c13','c14','c15','c16','c17','c18','c19','c20','c21','c22','c23','c24','c25','c26','c27','c28','c29','c30','c31','c32','c33','c34','c35','c36','c37','c38','c39','c40','c41'];
  displayedColumns: string[] = ['c1','c2','c3','c4','c5','c6','c7','c8','c3','c10','c11','c12','c13'];
  dataSource;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // @Input()orientation: 'horizontal' | 'vertical'
  
  posts;
  fileToupload;
  fileToupload1;
  cdate;
  constructor(private DatacallsService:DatacallsService,public snackBar:MatSnackBar) { }
 
  ngOnInit() {
    this.DatacallsService.viewFinsparcMarketPulseAdmin().subscribe(posts=>{
     this.posts=posts.result; 
     this.cdate=posts.result[0].cdate;
     console.log('current fle uploaded date -=-=>',this.cdate);
    console.log('posts data pulses-=-=>',this.posts);
    this.dataSource = new MatTableDataSource(this.posts);
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort

    })
    
  }
  onChange(fileInput: any) {
    this.fileToupload = fileInput.target.files['0']
  }
  onChange1(fileInput: any) {
    this.fileToupload1 = fileInput.target.files['0']
  }
  onSubmit() {
/************
 * select id,index_future,index_option,stock_future,stock_option,cdate from finsparc_marketpulse where cdate::date='2019-10-15'
 * ***************/
    var formData = new FormData();

    formData.append('file1', this.fileToupload);
    
    this.DatacallsService.upload_finsparc_marketpulse(formData).subscribe(posts => {
      console.log('posts--->',posts.result[0].status)
      if (posts.result[0].status==204) {
        this.snackBar.open('Please Upload Market Pulse VIX File First.', '', {
          duration: 2000
        });
        this.ngOnInit();
      }
      else if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploaded Successfully', '', {
          duration: 2000
        });
        this.ngOnInit();
      }
      else if(posts.result[0].status==203) 
      {
          this.snackBar.open('Your Column Name incorrect.', '', {
          duration: 2000
        });
      }
      else if(posts.result[0].status==404) 
      {
          this.snackBar.open('Please Choose File', '', {
          duration: 2000
        });
      }
      else 
      {
        
          this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 2000
        });
      }
    });


  }
  
  onSubmit1() {
 /*************
  *  select id,open,high,low,close,prev_close,change,per_change,cdate from finsparc_marketpulse where cdate::date='2019-10-15'
  * ************/
    var formData = new FormData();

    formData.append('file1', this.fileToupload1);
    
    console.log('Form Data -=-=->>',formData)
    this.DatacallsService.upload_finsparc_marketpulse_vix(formData).subscribe(posts => {

      if (posts['Status'] == 200) {
        this.snackBar.open('Excel Uploaded Successfully', '', {
          duration: 2000
        });

        this.ngOnInit();
      }
      else if(posts.result[0].status==203) 
      {
          this.snackBar.open('Column Name Incorrect', '', {
          duration: 2000
        });
      }
      else if(posts.result[0].status==404) 
      {
          this.snackBar.open('Please Choose File', '', {
          duration: 2000
        });
      }
      else 
      {
          this.snackBar.open('You Can not upload same Data Again', '', {
          duration: 2000
        });
      }
    });
  }

}
