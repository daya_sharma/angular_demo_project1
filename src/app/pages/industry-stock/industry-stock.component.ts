import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-industry-stock',
  templateUrl: './industry-stock.component.html',
  styleUrls: ['./industry-stock.component.scss'],
  providers:[DatacallsService]
})
export class IndustryStockComponent implements OnInit {
  displayedColumns: string[] = ['id','name','edit','delete'];
  dataSource;
  posts;
  id;  
  parent_id;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort :MatSort;
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 


  constructor(private DatacallsService:DatacallsService,private snackBar:MatSnackBar,private ActivatedRoute:ActivatedRoute) { }

  ngOnInit() { 

    this.id = this.ActivatedRoute.snapshot.params['id'];
    this.parent_id = this.ActivatedRoute.snapshot.params['parent_id'];
    console.log("patch value id -->", this.id);
    console.log("patch value parent_id -->", this.parent_id);

      this.DatacallsService.view_industry_stock(null,this.parent_id).subscribe(post=>{
        this.posts=post.result;
        console.log('posts data ======>',this.posts);

        this.dataSource=new MatTableDataSource(this.posts);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      })
  }

  onDelete(id){

    console.log("id-=-=-=-=-=->",id)
    var data={
      "id":id
    }
    console.log('data++++++>>',data)
    this.DatacallsService.universalDeleteAWS(data).subscribe(posts => {
      // this.posts = posts.result;
      this.snackBar.open('Notes Deleted Successfully', '', {
        duration: 2000
      });
      this.ngOnInit();
    });
    
}

}
