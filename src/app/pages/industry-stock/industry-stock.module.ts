import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndustryStockComponent } from './industry-stock.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes=[
  { path:'',component:IndustryStockComponent,pathMatch:'full'}
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [IndustryStockComponent],
  providers:[]
})
export class IndustryStockModule { }
