import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add-score-details',
  templateUrl: './add-score-details.component.html',
  styleUrls: ['./add-score-details.component.scss'],
  providers: [DatacallsService]
})
export class AddScoreDetailsComponent implements OnInit {

  scoreForm;
  post;
  id;
  post1;

  constructor(private FormBuilder: FormBuilder, private DatacallsService: DatacallsService, public snackBar: MatSnackBar, private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.scoreForm = this.FormBuilder.group({
      id: [0],
      particular: ['', Validators.required],
      case1_condition: ['', Validators.required],
      case1: ['', Validators.required],
      case2_condition: ['', Validators.required],
      case2: ['', Validators.required],
      case3_condition: ['', Validators.required],
      case3: ['', Validators.required],
      case4_condition: ['', Validators.required],
      case4: ['', Validators.required],
      case5_condition: ['', Validators.required],
      case5: ['', Validators.required],
      case6_condition: ['', Validators.required],
      case6: ['', Validators.required],
      case7_condition: ['', Validators.required],
      case7: ['', Validators.required],
      case8_condition: ['', Validators.required],
      case8: ['', Validators.required],
      case9_condition: ['', Validators.required],
      case9: ['', Validators.required],
    })
  }



  ngOnInit() {
    this.id = this.ActivatedRoute.snapshot.params['id'];

    console.log('fetched id  retrive --=-=->', this.id)
    if (this.id != null || this.id != undefined) {
      this.DatacallsService.patch_viewFinsparc_scorecal(this.id).subscribe(posts => {
        this.post1 = posts.result;
        // console.log('about us data retrive --=-=->',posts[0].id)
        console.log('about us data retrive --=-=->', this.post1)
        this.scoreForm.patchValue({
          id: posts.result[0].id,
          particular: posts.result[0].particular,
          case1_condition: posts.result[0].case1_condition == null || undefined ? '' : posts.result[0].case1_condition,
          case1: posts.result[0].case1 == null || undefined ? '' : posts.result[0].case1,
          case2_condition: posts.result[0].case2_condition == 'null' || undefined ? '' : posts.result[0].case2_condition,
          case2: posts.result[0].case2 == null || undefined ? '' : posts.result[0].case2,
          case3_condition: posts.result[0].case3_condition == 'null' || undefined ? '' : posts.result[0].case3_condition,
          case3: posts.result[0].case3 == null || undefined ? '' : posts.result[0].case3,
          case4_condition: posts.result[0].case3_condition == 'null' || undefined ? '' : posts.result[0].case3_condition,
          case4: posts.result[0].case4 == null || undefined ? '' : posts.result[0].case4,
          case5_condition: posts.result[0].case5_condition == 'null' || undefined ? '' : posts.result[0].case5_condition,
          case5: posts.result[0].case5 == null || undefined ? '' : posts.result[0].case5,
          case6_condition: posts.result[0].case6_condition == 'null' || undefined ? '' : posts.result[0].case6_condition,
          case6: posts.result[0].case6 == null || undefined ? '' : posts.result[0].case6,
          case7_condition: posts.result[0].case7_condition == 'null' || undefined ? '' : posts.result[0].case7_condition,
          case7: posts.result[0].case7 == null || undefined ? '' : posts.result[0].case7,
          case8_condition: posts.result[0].case8_condition == 'null' || undefined ? '' : posts.result[0].case8_condition,
          case8: posts.result[0].case8 == null || undefined ? '' : posts.result[0].case8,
          case9_condition: posts.result[0].case9_condition == 'null' || undefined ? '' : posts.result[0].case9_condition,
          case9: posts.result[0].case9 == null || undefined ? '' : posts.result[0].case9
        })

      })
    }
  }
  /*****
   * check in local Db function Name : viewfinsparcscoresnext();
   * *******/
  onSubmit() {
    var data = {
      "id": this.scoreForm.value.id,
      "particular": this.scoreForm.value.particular,
      "case1_condition": this.scoreForm.value.case1_condition == '' ? 'null' : this.scoreForm.value.case1_condition,
      "case1": this.scoreForm.value.case1 == '' ? null : this.scoreForm.value.case1,
      "case2_condition": this.scoreForm.value.case2_condition == '' ? 'null' : this.scoreForm.value.case2_condition,
      "case2": this.scoreForm.value.case2 == '' ? null : this.scoreForm.value.case2,
      "case3_condition": this.scoreForm.value.case3_condition == '' ? 'null' : this.scoreForm.value.case3_condition,
      "case3": this.scoreForm.value.case3 == '' ? null : this.scoreForm.value.case3,
      "case4_condition": this.scoreForm.value.case4_condition == '' ? 'null' : this.scoreForm.value.case4_condition,
      "case4": this.scoreForm.value.case4 == '' ? null : this.scoreForm.value.case4,
      "case5_condition": this.scoreForm.value.case5_condition == '' ? 'null' : this.scoreForm.value.case5_condition,
      "case5": this.scoreForm.value.case5=='' ? null :this.scoreForm.value.case5,
      "case6_condition": this.scoreForm.value.case6_condition=='' ? 'null': this.scoreForm.value.case6_condition,
      "case6": this.scoreForm.value.case6=='' ? null :this.scoreForm.value.case6,
      "case7_condition": this.scoreForm.value.case7_condition=='' ? 'null': this.scoreForm.value.case7_condition,
      "case7": this.scoreForm.value.case7==''? null :this.scoreForm.value.case7,
      "case8_condition": this.scoreForm.value.case8_condition=='' ? 'null': this.scoreForm.value.case8_condition,
      "case8": this.scoreForm.value.case8=='' ? null: this.scoreForm.value.case8,
      "case9_condition": this.scoreForm.value.case9_condition=='' ? 'null': this.scoreForm.value.case9_condition,
      "case9": this.scoreForm.value.case9== '' ? null: this.scoreForm.value.case9 
    }

    console.log('data com---=-=>', data)


    this.DatacallsService.insert_finsparc_scorecal(data).subscribe(posts => {
      this.post = posts;
      console.log("post data -=-=>", this.post)
      this.snackBar.open('Data Insertion Done Successfully', '', {
        duration: 2000
      });

      this.router.navigate(['/score-details']);

    })
  }

}
