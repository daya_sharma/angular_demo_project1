import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar } from '@angular/material';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers:[DatacallsService]
})
export class LoginComponent {
  public form:FormGroup;
  public settings: Settings;
  data;
  currentLoginUser;
  constructor(public appSettings:AppSettings, public fb: FormBuilder, public router:Router,public DatacallsService:DatacallsService,public snackBar:MatSnackBar){
    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
      // 'email': [null, Validators.compose([Validators.required, emailValidator])],
      // 'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])]
      'mobile': ['', Validators.required] ,
      'password': ['', Validators.required] 
    });
  }

  ngOnInit() {
    sessionStorage.getItem('currentuser')
    console.log('login user ',sessionStorage.getItem('currentuser'));

    if(sessionStorage.getItem('currentuser')!='undefined'){
      this.router.navigate(['/dashboard']);
    }
    else{
    this.router.navigate(['']);
      }

     }

  public onSubmit(values:Object):void {
    this.data={
          "mobile":this.form.value.mobile,
          "password":this.form.value.password
              }
    this.DatacallsService.verifyLogin(this.data).subscribe(posts=>{

      if (posts.Status == 200) {
      this.currentLoginUser={
                  "mobile":posts.Data[0].mobile,
                  "name":posts.Data[0].name,
                  "email":posts.Data[0].email,
                  "usertype":posts.Data[0].usertype
                }
    
      sessionStorage.setItem('currentuser', JSON.stringify(this.currentLoginUser));          
      console.log('login data found ',this.currentLoginUser);

      if (posts.Data[0].usertype == 'admin') {
        this.router.navigate(['/dashboard'])
      }
      
    }
    else {
      this.snackBar.open('Please Enter Valid Credentials !!', '', {
        duration: 2000
      });
    }

    })

  }

  ngAfterViewInit(){
    this.settings.loadingSpinner = false; 
  }
}