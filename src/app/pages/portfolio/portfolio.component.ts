import { Component, OnInit,ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import {DatacallsService} from '../../datacalls.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
  providers: [ DatacallsService ]  

})

export class PortfolioComponent implements OnInit {
  dataSource;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public displayedColumns = ['position', 'name'];
  // , 'weight', 'symbol','total'
  // public dataSource: any;
     
  constructor(public datacalls:DatacallsService,private activatedRoute: ActivatedRoute) { 
  
    // this.dataSource = new MatTableDataSource<Element>(this.tablesService.getData());

   }

  //  applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }
 user;
 posts;
  ngOnInit() {
    this.user=this.activatedRoute.snapshot.params['id'];
    console.log('user_id',this.user)
    this.datacalls.viewFinsparcPortFolio(this.user).subscribe(posts=>{
      this.posts=posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator;
      console.log('users',posts.result)
      });    
  }

}
