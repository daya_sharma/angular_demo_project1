import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-lot-change',
  templateUrl: './lot-change.component.html',
  styleUrls: ['./lot-change.component.scss'],
  providers: [DatacallsService]
})
export class LotChangeComponent implements OnInit {

  displayedColumns: string[] = ['id','designation','location','sub','sub1','sub2','modi','edit'];
  dataSource; 

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  posts;
  color1;
  constructor(private DataCallsService: DatacallsService, public snackBar: MatSnackBar) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  ngOnInit() {

    this.DataCallsService.view_finsparc_lotchange(null).subscribe(posts => {
      this.posts = posts.result;
      this.color1=this.posts[0].color
      console.log('posts lot change data ', this.posts);
      console.log('posts data color ', this.posts[0].color);
 
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort

    })

  }  

}
