import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LotChangeComponent } from './lot-change.component';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes=[
  { path:'',component:LotChangeComponent, pathMatch:'full'}
]
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [LotChangeComponent],
  providers:[]
})
export class LotChangeModule { }
