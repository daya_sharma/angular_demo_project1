import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { orders, products, customers, refunds } from '../dashboard.data';
import { DatacallsService } from 'src/app/datacalls.service';

@Component({
  selector: 'app-info-cards',
  templateUrl: './info-cards.component.html',
  styleUrls: ['./info-cards.component.scss'],
  providers:[DatacallsService]
})
export class InfoCardsComponent implements OnInit { 
  public orders: any[];
  public products: any[];
  public customers: any[];
  public refunds: any[];
  post;
  tot_bhav;
  prev_bhav;
  tot_marketpulse;
  tot_ivreport;
  prev_ivreport;
  tot_lots;
  uploaded_bhavcopy;
  uploaded_marketpulse_vix;
  uploaded_marketpulse;
  uploaded_iv_report;
  
  public colorScheme = {
    domain: ['#999']
  }; 
  public autoScale = true;
  @ViewChild('resizedDiv') resizedDiv:ElementRef;
  public previousWidthOfResizedDiv:number = 0; 
  public settings: Settings;
  constructor(public appSettings:AppSettings,public DatacallsService:DatacallsService){
    this.settings = this.appSettings.settings; 
  }

  ngOnInit(){
    this.orders = orders;
    this.products = products;
    this.customers = customers;
    this.refunds = refunds;
    this.orders = this.addRandomValue('orders');     
    this.customers = this.addRandomValue('customers');

console.log('hittttttt');
    this.DatacallsService.dashboard_data_count().subscribe(posts=>{
    this.post=posts.result[0];
      console.log('post data -=-=0=-=>>',this.post)
     this.tot_bhav=posts.result[0].totalbhavcopy;
     this.prev_bhav=posts.result[0].previous_bhavcopy_datacount;
     this.tot_marketpulse=posts.result[0].total_marketpulse_data;
     this.tot_ivreport=posts.result[0].iv_report_tot;
     this.prev_ivreport=posts.result[0].previous_day_ivreport;
    //  this.tot_lots=posts.results[0].tot_lots;
    this.uploaded_bhavcopy=posts.result[0].uploaded_bhavcopy; 
    this.uploaded_marketpulse_vix=posts.result[0].uploaded_marketpulse_vix; 
    this.uploaded_marketpulse=posts.result[0].uploaded_marketpulse; 
    this.uploaded_iv_report=posts.result[0].uploaded_iv_report; 

    })



  }
  
  public onSelect(event) {
    console.log(event);
  }

  public addRandomValue(param) {
    switch(param) {
      case 'orders':
        for (let i = 1; i < 30; i++) { 
          this.orders[0].series.push({"name": 1980+i, "value": Math.ceil(Math.random() * 1000000)});
        } 
        return this.orders;
      case 'customers':
        for (let i = 1; i < 15; i++) { 
          this.customers[0].series.push({"name": 2000+i, "value": Math.ceil(Math.random() * 1000000)});
        } 
        return this.customers;
      default:
        return this.orders;
    }
  }

  ngOnDestroy(){
    this.orders[0].series.length = 0;
    this.customers[0].series.length = 0;
  }

  ngAfterViewChecked() {    
    if(this.previousWidthOfResizedDiv != this.resizedDiv.nativeElement.clientWidth){
      setTimeout(() => this.orders = [...orders] ); 
      setTimeout(() => this.products = [...products] ); 
      setTimeout(() => this.customers = [...customers] ); 
      setTimeout(() => this.refunds = [...refunds] );
    }
    this.previousWidthOfResizedDiv = this.resizedDiv.nativeElement.clientWidth;
  }

}