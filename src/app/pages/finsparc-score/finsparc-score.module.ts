import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FinsparcScoreComponent } from './finsparc-score.component';


export const routes = [
  { path: '', component:FinsparcScoreComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule, 

  ],
  declarations: [FinsparcScoreComponent]
})
export class FinsparcScoreModule { }
