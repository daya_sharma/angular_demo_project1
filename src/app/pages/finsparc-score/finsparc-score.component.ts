import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatSnackBar} from '@angular/material';
import { DatacallsService } from 'src/app/datacalls.service';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

@Component({ 
  selector: 'app-finsparc-score',
  templateUrl: './finsparc-score.component.html',
  styleUrls: ['./finsparc-score.component.scss'],
  providers:[DatacallsService]
})
export class FinsparcScoreComponent implements OnInit {
  displayedColumns: string[] = ['id','designation','location','sub','sub2','sub1','sub3','edit'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  post;

  constructor(public DatacallsService:DatacallsService,public snackBar:MatSnackBar) { }
 
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    
  this.DatacallsService.viewFinsparc_score().subscribe(posts=>{
      this.post=posts.result;
      console.log('subData',this.post)
      this.dataSource = new MatTableDataSource(this.post);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })

  
  }

}
