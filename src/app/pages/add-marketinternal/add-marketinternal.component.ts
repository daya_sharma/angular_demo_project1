import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';


import * as moment from 'moment';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Route, Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-marketinternal',
  templateUrl: './add-marketinternal.component.html',
  styleUrls: ['./add-marketinternal.component.scss'],
  providers: [DatacallsService]
})
export class AddMarketinternalComponent implements OnInit {

  MarketInternalForm;
  post;
  posts;
  id;
  cat_id;
  subcat_id;
  key;
  constructor(private FormBuilder: FormBuilder, private DatacallService: DatacallsService, public snackBar: MatSnackBar, private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.MarketInternalForm = this.FormBuilder.group({
      id: [0],
      name: ['', Validators.required],
      // cat_id:[0],
      // subcat_id:[0] 
    })

  }

  ngOnInit() {
    this.id = this.ActivatedRoute.snapshot.params['id'];
    this.cat_id = this.ActivatedRoute.snapshot.params['cat_id'];
    this.subcat_id = this.ActivatedRoute.snapshot.params['subcat_id'];
    this.key = this.ActivatedRoute.snapshot.params['upkey'];
    console.log('fetch id-=-=>', this.id)
    console.log('fetch cat_id-=-=>', this.cat_id)
    console.log('fetch subcat_id-=-=>', this.subcat_id)
    console.log('fetch key-=-=>', this.key)

   if(this.id!=undefined && this.cat_id==undefined && this.subcat_id==undefined)
   {
      this.DatacallService.view_marketinternals(this.id,0,0).subscribe(posts => {
        this.posts = posts.result;
        console.log('this fetched data-=-=-=>>>>', this.posts)
        // this.dataSource = new MatTableDataSource(this.posts);
        // this.dataSource.paginator = this.paginator;
        // console.log('this posts this.posts[0].cdate-=-=-=>>>>',this.posts[0].cdate) 
        this.MarketInternalForm.patchValue({
          id: this.posts[0].id,
          name: this.posts[0].name
          // key:this.key
        })

      });
    }
    else if(this.id!=undefined && this.cat_id!=undefined && this.subcat_id==undefined)
    {
       this.DatacallService.view_marketinternals(this.id,this.cat_id,0).subscribe(posts => {
         this.posts = posts.result;
         console.log('this fetched data-=-=-=>>>>', this.posts)
         // this.dataSource = new MatTableDataSource(this.posts);
         // this.dataSource.paginator = this.paginator;
         // console.log('this posts this.posts[0].cdate-=-=-=>>>>',this.posts[0].cdate) 
         this.MarketInternalForm.patchValue({
           id: this.posts[0].id,
           name: this.posts[0].name
          //  key:this.key
         })
 
       });
     }
     else 
    {
       this.DatacallService.view_marketinternals(this.id,this.cat_id,this.subcat_id).subscribe(posts => {
         this.posts = posts.result;
         console.log('this fetched data-=-=-=>>>>', this.posts)
         // this.dataSource = new MatTableDataSource(this.posts);
         // this.dataSource.paginator = this.paginator;
         // console.log('this posts this.posts[0].cdate-=-=-=>>>>',this.posts[0].cdate) 
         this.MarketInternalForm.patchValue({
           id: this.posts[0].id,
           name: this.posts[0].name
          //  key:this.key
         })
 
       });
     }

  }

  onSubmit() {
    var data = {
      "id": this.MarketInternalForm.value.id,
      "name": this.MarketInternalForm.value.name,
      "cat_id": this.cat_id,
      "subcat_id": this.subcat_id,
    }
    console.log('data----=-=->', data)
    
    if(this.cat_id!=0 && this.subcat_id==undefined && this.key==undefined){
   
      this.DatacallService.insert_marketinternals(data).subscribe(posts => {
        this.post = posts;
        console.log('posts---> ', this.post);
        this.snackBar.open('Data Insertion Done Successfully', '', {
          duration: 2000
        });
  
        this.router.navigate(['/marketinternal']);
  
      })
    }
    else if(this.cat_id!=0 && this.subcat_id!=undefined && this.key==undefined){
   
      this.DatacallService.insert_marketinternals(data).subscribe(posts => {
        this.post = posts;
        console.log('posts---> ', this.post);
        this.snackBar.open('Data Updation Done Successfully', '', {
          duration: 2000
        });
  
        this.router.navigate(['/marketinternal']);
  
      })
    }
    else if(this.id!=undefined && this.cat_id==undefined && this.subcat_id==undefined && this.key==undefined){
    this.DatacallService.insert_marketinternals(data).subscribe(posts => {
      this.post = posts;
      console.log('posts---> ', this.post);
      this.snackBar.open('Data Insertion Done Successfully', '', {
        duration: 2000
      });

      this.router.navigate(['/marketinternal']);

    })

  }
  else 
  {
    console.log('else block executed');
    this.DatacallService.update_marketinternals(data).subscribe(posts => {
      this.post = posts;
      console.log('posts---> ', this.post);
      this.snackBar.open('Data Insertion Done Successfully', '', {
        duration: 2000
      });

      this.router.navigate(['/marketinternal']);

    })
  }


  }//submit close

}
