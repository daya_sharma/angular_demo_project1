import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.scss'],
  providers:[DatacallsService]
})
export class TradeComponent implements OnInit {
  displayedColumns: string[] = ['id','name','edit','delete'];
  dataSource;
  posts;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 

  constructor(private DatacallsService:DatacallsService,private snackBar:MatSnackBar) { }

  ngOnInit() {
  }

}
