import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

@Component({
  selector: 'app-score-details',
  templateUrl: './score-details.component.html',
  styleUrls: ['./score-details.component.scss'],
  providers:[DatacallsService]
})
export class ScoreDetailsComponent implements OnInit {
  displayedColumns: string[] = ['id','designation','d1','d2','d3','d4','d5','d6','d7','d8','d9','edit'];
  dataSource: MatTableDataSource<UserData>;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  post;

  constructor(private DatacallsService: DatacallsService, public snackBar: MatSnackBar) { }
 
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  ngOnInit() {
  
    this.DatacallsService.viewFinsparc_scorecal().subscribe(posts=>{
      this.post=posts.result;
      console.log('subData',this.post)
      this.dataSource = new MatTableDataSource(this.post);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })

  
  }
    

  }


