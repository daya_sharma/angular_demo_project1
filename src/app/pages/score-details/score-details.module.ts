import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {ScoreDetailsComponent} from './score-details.component';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes=[
  { path:'',component:ScoreDetailsComponent, pathMatch:'full'}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule 
  ],
  declarations: [ScoreDetailsComponent],
  providers:[]
})
export class ScoreDetailsModule { }
