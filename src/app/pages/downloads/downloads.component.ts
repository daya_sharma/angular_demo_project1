import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar, MatButton } from '@angular/material';

export interface PeriodicElement {
  name: string;
  position: number;

}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Daily Future Report' },
  { position: 2, name: 'Score Report' },
  { position: 3, name: 'OI Changes' },
  { position: 4, name: 'FNO Reports' },
  { position: 5, name: 'Score Card' },
  { position: 6, name: 'Significant Changes in OI' },
  // { position: 7, name: 'OI Changes' },
  { position: 8, name: 'Adding Most OI' },
  { position: 9, name: 'Shedding Most OI' },
  { position: 10, name: 'IV Report' },
  { position: 11, name: 'High Report' },
  { position: 12, name: 'Low Report' },
  { position: 13, name: 'Option Call Data' },
  { position: 14, name: 'Option Put Data' },
  { position: 15, name: '11 Days High Report' },
  { position: 16, name: '11 Days Low Report' },
  { position: 17, name: 'Future Report 1' },
  { position: 18, name: 'Future Report 2' },
  { position: 19, name: 'Option Report' },
  { position: 20, name: 'MIS Report' },
  { position: 21, name: 'Score details Near Expiry Report' },
  { position: 22, name: 'Score details Next Expiry Report' },
  { position: 23, name: 'Score Next Expiry Report' }
];

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.scss'],
  providers: [DatacallsService]
})
export class DownloadsComponent implements OnInit {

  displayedColumns = ['position', 'name', 'download'];
  dataSource = ELEMENT_DATA;

  // favoriteSeason: string;
  // seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  id;
  selectedItem;

  constructor(public DatacallsService: DatacallsService, public snackBar: MatSnackBar) { }

  ngOnInit() {


  }
  onChange(event) {
    // let selectedId=event.value;
    console.log('selected data --->>', event);
    //  this.onSubmit(event.value);
    switch (event) {
      case 1: {

        console.log("1 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.viewfinsparccallputvaluexceldownload().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "DailyFutureReport.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });

        break;
      }
      case 2: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.score_reports_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Score_report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });

        break;
      }
      case 3: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.oi_chnages_reports_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "OI_changes_report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });

        break;
      }
      case 4: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.download_FNO_score_near().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "FNO_report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });
        break;
      }
      case 5: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.score_card_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Score_Card_report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });

        break;
      }
      case 6: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.significant_changes_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Significant-Changes-In-OI.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });
        break;
      }
      case 7: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.oi_changes_futures_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "OI-Changes.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });
        break;
      }
      case 8: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });

        this.DatacallsService.adding_most_oi_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Adding-Most_OI.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });

        });

        break;
      }
      case 9: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.shedding_oi_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Shedding-Most_OI.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });

        break;
      }
      case 10: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.iv_report_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "IV-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 11: {

        console.log("2 ex");
        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.high_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "high-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 12: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.low_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Low-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 13: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.high_7_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "OptionCallData .xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 14: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.low_7_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "OptionPutData.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 15: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.high_11_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "11-Days-High-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 16: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.low_11_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "11-Days-Low-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 17: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.future_report_1_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "future_report_1.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 18: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.future_report_2_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "future_report_2.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 19: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 3000
        });
        this.DatacallsService.option_report_download().subscribe(data => {
          // this.posts = posts.result;
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Option_Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 20: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 133800
        });
        this.DatacallsService.mis_report_download().subscribe(data => {
          // this.posts = posts.result;
          console.log('20 Data',data);
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "MIS-Report.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      case 21: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 133800
        });
        this.DatacallsService.bf_score_reports_download_near().subscribe(data => {
          // this.posts = posts.result;
          console.log('20 Data',data);
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "BFScore-NearExpiry-Details.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 22: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 133800
        });
        this.DatacallsService.bf_score_reports_download().subscribe(data => {
          // this.posts = posts.result;
          console.log('20 Data',data);
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "BFScore-NextExpiry-Details.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }
      case 23: {

        this.snackBar.open('Be Patience , Your file is Downloading........', '', {
          duration: 133800
        });
        this.DatacallsService.score_reports_download_next().subscribe(data => {
          // this.posts = posts.result;
          console.log('20 Data',data);
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(data);
          link.download = "Score-Report-NextExpiry.xlsx";
          link.click();
          this.snackBar.open('File is downloaded .....', '', {
            duration: 3000
          });
    
        });
        break;
      }

      default: {

        break;
      }
    }

  }

  onSubmit(id) {
    if (id == 1) {
      this.selectedItem = 'data 1';
      this.DatacallsService.download_FNO_score_near().subscribe(data => {
        // this.posts = posts.result;
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(data);
        link.download = "FNO_Score_near.xlsx";
        link.click();

      });
    }
    if (id == 2) {
      this.selectedItem = 'data 2';

    }
    if (id == 3) {
      this.selectedItem = 'data 3';
      this.DatacallsService.download_oi_changes_near().subscribe(data => {
        // this.posts = posts.result;
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(data);
        link.download = "OI_Changes_Near.xlsx";
        link.click();
      })

    }
    console.log('onSubmit() finction data ', id)
  }

  onCallPut_download() {

    this.DatacallsService.viewfinsparccallputvaluexceldownload().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "callputvalue.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });

  }

  scoreReport_download() {

    this.DatacallsService.score_reports_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "Score_report.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });

  }

  OI_Changes_Download() {

    this.DatacallsService.oi_chnages_reports_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "OI_changes_report.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // download_FNO_score_near
  FNO_download() {

    this.DatacallsService.download_FNO_score_near().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "FNO_report.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // score_card_download
  score_Card_download() {

    this.DatacallsService.score_card_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "Score_Card_report.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // significant_changes_download
  significant_changes_download() {

    this.DatacallsService.significant_changes_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "Significant-Changes-In-OI.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // significant_changes_download
  oi_changes_download() {

    this.DatacallsService.oi_changes_futures_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "OI-Changes.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // adding_most_oi_download
  adding_most_oi_download() {

    this.DatacallsService.adding_most_oi_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "Adding-Most_OI.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  // shedding_oi_download
  shedding_oi_download() {
    this.DatacallsService.shedding_oi_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "Shedding-Most_OI.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

  iv_report_download() {
    this.DatacallsService.iv_report_download().subscribe(data => {
      // this.posts = posts.result;
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(data);
      link.download = "IV-Report.xlsx";
      link.click();
      this.snackBar.open('File will be downloaded in a movement, Please Wait.....', '', {
        duration: 3000
      });

    });
  }

}
