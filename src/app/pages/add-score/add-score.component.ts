import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-score',
  templateUrl: './add-score.component.html',
  styleUrls: ['./add-score.component.scss'],
  providers:[DatacallsService]
})
export class AddScoreComponent implements OnInit {
scoreForm;
post;
id;
post1;
  constructor(private FormBuilder:FormBuilder,private DatacallsService:DatacallsService,public snackBar:MatSnackBar,private router:Router,private ActivatedRoute:ActivatedRoute) { 
    this.scoreForm = this.FormBuilder.group({
      id:[0],
      pricedesc:['',Validators.required],
      minrange:['',Validators.required],
      maxrange:['',Validators.required],
      maxscore:['',Validators.required],
      interval:['',Validators.required],  
      score:['',Validators.required],  
    }) 
  }

  ngOnInit() {
    this.id=this.ActivatedRoute.snapshot.params['finsparc_score_id'];
 
    console.log('fetched id  retrive --=-=->',this.id)
  if(this.id!=null || this.id!=undefined){
    this.DatacallsService.patch_viewFinsparc_score(this.id).subscribe(posts=>{
      this.post1=posts.result;
      // console.log('about us data retrive --=-=->',posts[0].id)
      console.log('about us data retrive --=-=->',this.post1)
      this.scoreForm.patchValue({
          id:posts.result[0].finsparc_score_id,
          pricedesc:posts.result[0].price_desc,
          minrange:posts.result[0].min_range,
          maxrange:posts.result[0].max_range,
          maxscore:posts.result[0].max_score,
          interval:posts.result[0].intval,
          score:posts.result[0].score
      })
    
    })
   }

  }

  onSubmit(){
    var data={
      "id":this.scoreForm.value.id,
      "price_desc":this.scoreForm.value.pricedesc,
      "min_range":this.scoreForm.value.minrange,
      "max_range":this.scoreForm.value.maxrange,
      "max_score":this.scoreForm.value.maxscore,
      "interval":this.scoreForm.value.interval,
      "score":this.scoreForm.value.score
    }

    console.log('data com---=-=>',data) 
    

    this.DatacallsService.insert_finsparc_score(data).subscribe(posts=>{
      this.post=posts;
      console.log("post data -=-=>",this.post)
      this.snackBar.open('Data Insertion Done Successfully', '', {
        duration: 2000
      });
      
      this.router.navigate(['/finsparcscore']);

    })
  }

}
