import { Component, OnInit, ViewEncapsulation,ViewChild } from '@angular/core';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import {ClickEventArgs} from '@syncfusion/ej2-angular-navigations';
import { DatacallsService } from '../../datacalls.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
// import{orderDetails} from './data';


@Component({
  selector: 'app-mytable',
  templateUrl: './mytable.component.html',
  styleUrls: ['./mytable.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class MytableComponent implements OnInit {
  public data: Object[];
  // data;
  @ViewChild('grid')
    public grid: GridComponent;
    public toolbar: Object[];

  constructor() { }

  ngOnInit() {
    // this.DatacallsService.viewfinsparcmarketpulse().subscribe(posts=>{
    //   this.data=posts.result;
    //   console.log('posts data pulses-=-=>',this.data);
    // })
    this.toolbar = [
      { prefixIcon: 'e-small-icon', id: 'big', align: 'Right' },
      { prefixIcon: 'e-medium-icon', id: 'medium', align: 'Right' },
      { prefixIcon: 'e-big-icon', id: 'small', align: 'Right' }
      ];
  }


  clickHandler(args: ClickEventArgs): void {
    if (args.item.id === 'small') {
        this.grid.rowHeight = 20;
        }
    if (args.item.id === 'medium') {
        this.grid.rowHeight = 40;
    }
    if (args.item.id === 'big') {
        this.grid.rowHeight = 60;
    }
}


}
