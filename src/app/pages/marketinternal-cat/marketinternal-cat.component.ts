import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar,MatTableDataSource,MatPaginator,MatSort} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-marketinternal-cat',
  templateUrl: './marketinternal-cat.component.html',
  styleUrls: ['./marketinternal-cat.component.scss'],
  providers:[DatacallsService]
})
export class MarketinternalCatComponent implements OnInit {
  posts;
  displayedColumns: string[] = ['id','name','edit','view'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cat_id;

  constructor(private Datacallservice:DatacallsService,public snackBar:MatSnackBar,private ActivatedRoute:ActivatedRoute,private router:Router,private FormBuilder:FormBuilder) { }

  ngOnInit() {
    this.cat_id = this.ActivatedRoute.snapshot.params['id'];
    console.log('cat posts---=-=-=-->>',this.cat_id)
    this.Datacallservice.view_marketinternals(null,this.cat_id,0).subscribe(posts => {
      this.posts = posts.result
      console.log('cat posts---=-=-=-->>',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort

    });

  }


}
