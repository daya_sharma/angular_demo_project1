import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { FormBuilder,Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-dynamic-options',
  templateUrl: './add-dynamic-options.component.html',
  styleUrls: ['./add-dynamic-options.component.scss'],
  providers:[DatacallsService]
})
export class AddDynamicOptionsComponent implements OnInit {

  DOForm;
  id;
  post1;
  post;
  constructor(private FormBuilder:FormBuilder,private DatacallsService:DatacallsService,public snackBar:MatSnackBar,private router:Router,private ActivatedRoute:ActivatedRoute) { 
    
    this.DOForm = this.FormBuilder.group({
      id:[0],
      name:['',Validators.required],
      value:['',Validators.required],
      // maxrange:['',Validators.required],
      // maxscore:['',Validators.required],
      // interval:['',Validators.required],  
      // score:['',Validators.required],  
    })
  }

  ngOnInit() {
    this.id=this.ActivatedRoute.snapshot.params['id'];
    console.log('fetched id  retrive --=-=->',this.id)
  
    if(this.id!=null || this.id!=undefined){
    this.DatacallsService.patch_view_finsparc_dynamicoptions(this.id).subscribe(posts=>{
      this.post1=posts.result;
      // console.log('about us data retrive --=-=->',posts[0].id)
      console.log('about us data retrive --=-=->',this.post1)
      this.DOForm.patchValue({
          id:posts.result[0].id,
          name:posts.result[0].name,
          value:posts.result[0].value,
        
      })
    
    })
   }

  }

  // insert_finsparc_dynamicoptions
  onSubmit(){
    var data={
      "id":this.DOForm.value.id,
      "name":this.DOForm.value.name,
      "value":this.DOForm.value.value,
    }

    console.log('data com---=-=>',data) 
    

    this.DatacallsService.insert_finsparc_dynamicoptions(data).subscribe(posts=>{
      this.post=posts;
      console.log("post data -=-=>",this.post)
      this.snackBar.open('Data Insertion Done Successfully', '', {
        duration: 2000
      });
      
      this.router.navigate(['/dynamic-option']);

    })
  }

 

}
