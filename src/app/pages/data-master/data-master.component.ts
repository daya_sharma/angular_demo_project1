import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatPaginator, MatSort, MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-data-master',
  templateUrl: './data-master.component.html',
  styleUrls: ['./data-master.component.scss'],
  providers:[DatacallsService]
})
export class DataMasterComponent implements OnInit {
  displayedColumns: string[] = [ 'name', 'id', 'd1','d2','d4','d5','d6','d7','d11','d8','d9','d10','d12','d13','d14','d15'];
  dataSource;
  posts;

@ViewChild(MatPaginator)paginator:MatPaginator;
@ViewChild(MatSort)sort:MatSort;
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.firstPage();
  }
}
  constructor(private DatacallsService:DatacallsService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.DatacallsService.view_fno_score_near_data().subscribe(post=>{
      this.posts=post.result;
      console.log('posts data-=-=->>',this.posts);

      this.dataSource=new MatTableDataSource(this.posts);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

    })

  }

  download(){
    console.log('inside download..')
    this.DatacallsService.download_FNO_score_near().subscribe(data => {
      // this.posts = posts.result;
      var link=document.createElement('a');
        link.href=window.URL.createObjectURL(data);
        link.download="FNO_Score_near.xlsx";
        link.click();

    }); 
  }

}
