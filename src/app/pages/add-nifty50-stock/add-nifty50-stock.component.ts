import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-add-nifty50-stock',
  templateUrl: './add-nifty50-stock.component.html',
  styleUrls: ['./add-nifty50-stock.component.scss'],
  providers:[DatacallsService]
})
export class AddNifty50StockComponent implements OnInit {
  id;
  lotForm;
  post;
  posts;
  parent_id;


  constructor(private DatacallsService:DatacallsService,private ActivatedRoute:ActivatedRoute,private FormBuilder:FormBuilder,public snackBar:MatSnackBar,private router:Router) { 
    this.lotForm = this.FormBuilder.group({
      id: [0],
      name: [''], 
      // adjust_factor: ['', Validators.required],
      // change_type: ['', Validators.required],
      // lot_date: ['', Validators.required]
    })

  }

  ngOnInit() {
    this.id = this.ActivatedRoute.snapshot.params['id'];
    console.log("patch value id -->", this.id);
    this.parent_id = this.ActivatedRoute.snapshot.params['parent_id'];
    console.log("patch value parent_id -->", this.parent_id);

    if (this.id != 'undefined') {
      this.DatacallsService.view_nifty50_stock(this.id,this.parent_id).subscribe(posts => {
        this.posts = posts.result
        console.log('posts---=>',this.posts);
       
        this.lotForm.patchValue({  
          id: this.posts[0].id,
          name: this.posts[0].name,
          
        })

      });
    }

  }

  onSubmit(){
    var data={
                "id":this.lotForm.value.id,
                "parent_id":this.parent_id,
                "name":this.lotForm.value.name
             }
    console.log('this data -=-=-=>',data)
        this.DatacallsService.insert_nifty50_stock(data).subscribe(posts=>{
          this.post=posts;
          console.log('posts---> ',this.post);
          this.snackBar.open('Data Insertion Done Successfully', '', {
            duration: 2000
          });
          
          this.router.navigate(['/nifty50-stock/']);
        })
      }

}
