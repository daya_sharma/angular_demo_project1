import { Component, OnInit,ViewChild} from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { MatSnackBar, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-dynamic-options',
  templateUrl: './dynamic-options.component.html',
  styleUrls: ['./dynamic-options.component.scss'],
  providers:[DatacallsService]
})
export class DynamicOptionsComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'symbol','edit'];

  dataSource;
  posts;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private DatacallsService:DatacallsService,public snackBar:MatSnackBar) { }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {

    this.DatacallsService.view_finsparc_dynamicoptions().subscribe(posts => {
      this.posts = posts.result

      console.log('posts data dynamic option ----> ',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    });
  }

  onDelete(id){

    console.log("id-=-=-=-=-=->",id)
    var data={
      "id":id,
      "table_id":"id",
      "table":"finsparc_dynamicoptions"
    }

    this.DatacallsService.universalDelete(data).subscribe(posts => {
      // this.posts = posts.result;
      this.snackBar.open('Notes Deleted Successfully', '', {
        duration: 2000
      });
      this.ngOnInit();
    });
    
}

}
