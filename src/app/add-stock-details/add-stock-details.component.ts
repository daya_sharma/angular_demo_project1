import { Component, OnInit } from '@angular/core';
import { DatacallsService } from 'src/app/datacalls.service';
import { FormBuilder,Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-stock-details',
  templateUrl: './add-stock-details.component.html',
  styleUrls: ['./add-stock-details.component.scss'],
  providers:[DatacallsService]
})
export class AddStockDetailsComponent implements OnInit {

  DOForm;
  id;
  post1;
  post;

  constructor(private FormBuilder:FormBuilder,private DatacallsService:DatacallsService,public snackBar:MatSnackBar,private router:Router,private ActivatedRoute:ActivatedRoute) {
    
    this.DOForm = this.FormBuilder.group({
      id:[0],
      stock:['',Validators.required],
      score_type:['',Validators.required],
      // rank_change:['',Validators.required],
      // rank_change_date:['',Validators.required]
      // maxrange:['',Validators.required],
      // maxscore:['',Validators.required],
      // interval:['',Validators.required],  
      // score:['',Validators.required],  
    })

   }

  ngOnInit() {
    
    this.id=this.ActivatedRoute.snapshot.params['id'];
    console.log('fetched id  retrive --=-=->',this.id)
  
    if(this.id!=null || this.id!=undefined){
    this.DatacallsService.patch_view_finsparc_stockname(this.id).subscribe(posts=>{
      this.post1=posts.result;
      // console.log('about us data retrive --=-=->',posts[0].id)
      console.log('about us data retrive --=-=->',this.post1)
      this.DOForm.patchValue({
          id:posts.result[0].id,
          stock:posts.result[0].stock,
          score_type:posts.result[0].score_type,
          // rank_change:posts.result[0].rank_change,
          // rank_change_date:posts.result[0].rank_change_date 
      })
    
    })
   }
  
   
 
  }

    onSubmit(){
      var data={
        "id":this.DOForm.value.id,
        "stock":this.DOForm.value.stock,
        "score_type":this.DOForm.value.score_type,
        // "rank_change":this.DOForm.value.rank_change,
        // "rank_change_date":this.DOForm.value.rank_change_date,
      }
  
      console.log('data com---=-=>',data) 
      
  
      this.DatacallsService.insert_finsparc_stockname(data).subscribe(posts=>{
        this.post=posts;
        console.log("post data -=-=>",this.post)
        this.snackBar.open('Data Insertion Done Successfully', '', {
          duration: 2000
        });
        
        this.router.navigate(['/stock-details']);
  
      })
    }

}
