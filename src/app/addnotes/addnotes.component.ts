import { Component, OnInit, ViewChild, Inject, ViewEncapsulation } from '@angular/core';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, RichTextEditorComponent, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { createElement, MouseEventArgs, addClass, removeClass, Browser } from '@syncfusion/ej2-base';
import CodeMirror from 'codemirror';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/css/css.js';
import 'codemirror/mode/htmlmixed/htmlmixed.js';

import { DatacallsService } from '../datacalls.service';
// import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
// import { DatacallsService } from '../datacalls.service';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-addnotes',
  templateUrl: './addnotes.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./over-view.css'],
  providers: [DatacallsService]
})
export class AddnotesComponent implements OnInit {
  rteForm;
  // id;
  // msg;
  id;
  posts;
  dataSource;
  constructor(private _fb: FormBuilder,private sanitizer: DomSanitizer, private activatedRoute: ActivatedRoute, private _http: Http, private Router: Router, private _datacalls: DatacallsService, public snackBar: MatSnackBar) { 
    this.rteForm = this._fb.group({
      id: [0],
      // market_mood_index:[''],
      notes1: [''],
      notes2: [''],
      notes3: [''],
      notes4: [''],
      notes: [''],
      cdate:['', Validators.required],
      usertype:['',Validators.required]

    })
  }

  ngOnInit() {
    
    this.id=this.activatedRoute.snapshot.params['finsparc_editors_notes_id'];
    console.log('id',this.id)
    if(this.id!='undefined')
    {
    this._datacalls.viewFinsparcEditorsNotes1(this.id).subscribe(posts=>{
      this.posts=posts.result;
      console.log('this posts view data-=-=-=>>>>',this.posts)
      // this.dataSource = new MatTableDataSource(this.posts);
      // this.dataSource.paginator = this.paginator;
      // console.log('this posts this.posts[0].cdate-=-=-=>>>>',this.posts[0].cdate) 
      this.rteForm.patchValue({
        id:this.posts[0].finsparc_editors_notes_id,
        // market_mood_index:this.posts[0].mood_of_market,
        notes1:this.posts[0].description1,
        notes2:this.posts[0].description2,
        notes3:this.posts[0].description3,
        notes4:this.posts[0].description4,
        notes:this.posts[0].description,
        cdate:moment(this.posts[0].cdate).format('YYYY-MM-DD'),
        usertype:this.posts[0].type
      })
   
      console.log('the patch value data',this.posts[0].cdate);

      }); 
    }
  }

  onSubmit(){
  var formData = {};

    formData={  
      "id":this.rteForm.value.id,
      // "market_mood_index":this.rteForm.value.market_mood_index,
      "description1": this.rteForm.value.notes1,
      "description2": this.rteForm.value.notes2,
      "description3": this.rteForm.value.notes3,
      "description4": this.rteForm.value.notes4,
      "description": this.rteForm.value.notes,
      "cdate":moment(this.rteForm.value.cdate).format('YYYY-MM-DD'),
      "type":this.rteForm.value.usertype
    }   

    console.log("formData-=-=-=-=-=-=->>",formData);
if(this.rteForm.value.notes!=undefined && this.rteForm.value.notes!=""){
    this._datacalls.insertFinsparcEditorsNotes(formData).subscribe(posts => {
      this.snackBar.open('Notes Added Successfully', '', {
        duration: 2000
      });
      window.setTimeout(() => {
        this.Router.navigate(['/editornotes']);
      }, 2000);

      console.log('Done');
    });
    }
    else{
      // console.log('Please Enter Some Input !');
      this.snackBar.open('Please Enter Some Input !', '', {
        duration: 2000
      });
    }
  }

  @ViewChild('toolsRTE') public rteObj: RichTextEditorComponent;
  public tools: object = {
      items: ['Bold', 'Italic', 'Underline', 'StrikeThrough',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor',
      'LowerCase', 'UpperCase', '|',
      'Formats', 'Alignments', 'OrderedList', 'UnorderedList',
      'Outdent', 'Indent', '|',
      'CreateTable', 'CreateLink', 'Image', '|', 'ClearFormat', 'Print',
      'SourceCode', 'FullScreen', '|', 'Undo', 'Redo']
  };
  public maxLength: number = 100000;
  public textArea: HTMLElement;
  public myCodeMirror: any;
ngAfterViewInit(): void {
  let rteObj: RichTextEditorComponent = this.rteObj;
  setTimeout(() => { this.textArea = rteObj.contentModule.getEditPanel() as HTMLElement; }, 600);
}
public mirrorConversion(e?: any): void {
  let id: string = this.rteObj.getID() + 'mirror-view';
  let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
  let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
  if (e.targetItem === 'Preview') {
      this.textArea.style.display = 'block';
      mirrorView.style.display = 'none';
      this.textArea.innerHTML = this.myCodeMirror.getValue();
      charCount.style.display = 'block';
  } else {
      if (!mirrorView) {
          mirrorView = createElement('div', { className: 'e-content' });
          mirrorView.id = id;
          this.textArea.parentNode.appendChild(mirrorView);
      } else {
          mirrorView.innerHTML = '';
      }
      this.textArea.style.display = 'none';
      mirrorView.style.display = 'block';
      this.renderCodeMirror(mirrorView, this.rteObj.value);
      charCount.style.display = 'none';
  }
}

public renderCodeMirror(mirrorView: HTMLElement, content: string): void {
  this.myCodeMirror = CodeMirror(mirrorView, {
      value: content,
      lineNumbers: true,
      mode: 'text/html',
      lineWrapping: true,

  });
}

public mirrorConversion1(e?: any): void {
  let id: string = this.rteObj.getID() + 'mirror-view';
  let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
  let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
  if (e.targetItem === 'Preview') {
      this.textArea.style.display = 'block';
      mirrorView.style.display = 'none';
      this.textArea.innerHTML = this.myCodeMirror.getValue();
      charCount.style.display = 'block';
  } else {
      if (!mirrorView) {
          mirrorView = createElement('div', { className: 'e-content' });
          mirrorView.id = id;
          this.textArea.parentNode.appendChild(mirrorView);
      } else {
          mirrorView.innerHTML = '';
      }
      this.textArea.style.display = 'none';
      mirrorView.style.display = 'block';
      this.renderCodeMirror1(mirrorView, this.rteObj.value);
      charCount.style.display = 'none';
  }
}

public renderCodeMirror1(mirrorView: HTMLElement, content: string): void {
  this.myCodeMirror = CodeMirror(mirrorView, {
      value: content,
      lineNumbers: true,
      mode: 'text/html',
      lineWrapping: true,

  });
}

public mirrorConversion2(e?: any): void {
  let id: string = this.rteObj.getID() + 'mirror-view';
  let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
  let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
  if (e.targetItem === 'Preview') {
      this.textArea.style.display = 'block';
      mirrorView.style.display = 'none';
      this.textArea.innerHTML = this.myCodeMirror.getValue();
      charCount.style.display = 'block';
  } else {
      if (!mirrorView) {
          mirrorView = createElement('div', { className: 'e-content' });
          mirrorView.id = id;
          this.textArea.parentNode.appendChild(mirrorView);
      } else {
          mirrorView.innerHTML = '';
      }
      this.textArea.style.display = 'none';
      mirrorView.style.display = 'block';
      this.renderCodeMirror2(mirrorView, this.rteObj.value);
      charCount.style.display = 'none';
  }
}

public renderCodeMirror2(mirrorView: HTMLElement, content: string): void {
  this.myCodeMirror = CodeMirror(mirrorView, {
      value: content,
      lineNumbers: true,
      mode: 'text/html',
      lineWrapping: true,

  });
}

public mirrorConversion3(e?: any): void {
  let id: string = this.rteObj.getID() + 'mirror-view';
  let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
  let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
  if (e.targetItem === 'Preview') {
      this.textArea.style.display = 'block';
      mirrorView.style.display = 'none';
      this.textArea.innerHTML = this.myCodeMirror.getValue();
      charCount.style.display = 'block';
  } else {
      if (!mirrorView) {
          mirrorView = createElement('div', { className: 'e-content' });
          mirrorView.id = id;
          this.textArea.parentNode.appendChild(mirrorView);
      } else {
          mirrorView.innerHTML = '';
      }
      this.textArea.style.display = 'none';
      mirrorView.style.display = 'block';
      this.renderCodeMirror3(mirrorView, this.rteObj.value);
      charCount.style.display = 'none';
  }
}

public mirrorConversion4(e?: any): void {
  let id: string = this.rteObj.getID() + 'mirror-view';
  let mirrorView: HTMLElement = this.rteObj.element.querySelector('#' + id) as HTMLElement;
  let charCount: HTMLElement = this.rteObj.element.querySelector('.e-rte-character-count') as HTMLElement;
  if (e.targetItem === 'Preview') {
      this.textArea.style.display = 'block';
      mirrorView.style.display = 'none';
      this.textArea.innerHTML = this.myCodeMirror.getValue();
      charCount.style.display = 'block';
  } else {
      if (!mirrorView) {
          mirrorView = createElement('div', { className: 'e-content' });
          mirrorView.id = id;
          this.textArea.parentNode.appendChild(mirrorView);
      } else {
          mirrorView.innerHTML = '';
      }
      this.textArea.style.display = 'none';
      mirrorView.style.display = 'block';
      this.renderCodeMirror4(mirrorView, this.rteObj.value);
      charCount.style.display = 'none';
  }
}

public renderCodeMirror4(mirrorView: HTMLElement, content: string): void {
  this.myCodeMirror = CodeMirror(mirrorView, {
      value: content,
      lineNumbers: true,
      mode: 'text/html',
      lineWrapping: true,

  });
}

public renderCodeMirror3(mirrorView: HTMLElement, content: string): void {
  this.myCodeMirror = CodeMirror(mirrorView, {
      value: content,
      lineNumbers: true,
      mode: 'text/html',
      lineWrapping: true,

  });
}

public handleFullScreen(e: any): void {
  let leftBar: HTMLElement = document.querySelector('#left-sidebar');
  if (e.targetItem === 'Maximize') {
      addClass([leftBar], ['e-close']);
      removeClass([leftBar], ['e-open']);
  } else if (e.targetItem === 'Minimize') {
      removeClass([leftBar], ['e-close']);
      if (!Browser.isDevice) {
      addClass([leftBar], ['e-open']); }
  }
}
public handleFullScreen1(e: any): void {
  let leftBar: HTMLElement = document.querySelector('#left-sidebar');
  if (e.targetItem === 'Maximize') {
      addClass([leftBar], ['e-close']);
      removeClass([leftBar], ['e-open']);
  } else if (e.targetItem === 'Minimize') {
      removeClass([leftBar], ['e-close']);
      if (!Browser.isDevice) {
      addClass([leftBar], ['e-open']); }
  }
}
public handleFullScreen2(e: any): void {
  let leftBar: HTMLElement = document.querySelector('#left-sidebar');
  if (e.targetItem === 'Maximize') {
      addClass([leftBar], ['e-close']);
      removeClass([leftBar], ['e-open']);
  } else if (e.targetItem === 'Minimize') {
      removeClass([leftBar], ['e-close']);
      if (!Browser.isDevice) {
      addClass([leftBar], ['e-open']); }
  }
}
public handleFullScreen3(e: any): void {
  let leftBar: HTMLElement = document.querySelector('#left-sidebar');
  if (e.targetItem === 'Maximize') {
      addClass([leftBar], ['e-close']);
      removeClass([leftBar], ['e-open']);
  } else if (e.targetItem === 'Minimize') {
      removeClass([leftBar], ['e-close']);
      if (!Browser.isDevice) {
      addClass([leftBar], ['e-open']); }
  }
}
public handleFullScreen4(e: any): void {
  let leftBar: HTMLElement = document.querySelector('#left-sidebar');
  if (e.targetItem === 'Maximize') {
      addClass([leftBar], ['e-close']);
      removeClass([leftBar], ['e-open']);
  } else if (e.targetItem === 'Minimize') {
      removeClass([leftBar], ['e-close']);
      if (!Browser.isDevice) {
      addClass([leftBar], ['e-open']); }
  }
}
public actionCompleteHandler(e: any): void {
  if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
      (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
      this.mirrorConversion(e);
  } else {
      setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
  }
}
public actionCompleteHandler1(e: any): void {
  if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
      (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
      this.mirrorConversion1(e);
  } else {
      setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
  }
}
public actionCompleteHandler2(e: any): void {
  if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
      (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
      this.mirrorConversion2(e);
  } else {
      setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
  }
}
public actionCompleteHandler3(e: any): void {
  if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
      (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
      this.mirrorConversion3(e);
  } else {
      setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
  }
}
public actionCompleteHandler4(e: any): void {
  if (e.targetItem && (e.targetItem === 'SourceCode' || e.targetItem === 'Preview')) {
      (this.rteObj.sourceCodeModule.getPanel() as HTMLTextAreaElement).style.display = 'none';
      this.mirrorConversion4(e);
  } else {
      setTimeout(() => { this.rteObj.toolbarModule.refreshToolbarOverflow(); }, 400);
  }
}
}
