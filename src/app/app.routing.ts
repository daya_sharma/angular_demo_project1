import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { BlankComponent } from './pages/blank/blank.component';
import { SearchComponent } from './pages/search/search.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { ErrorComponent } from './pages/errors/error/error.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { InportfolioComponent } from './pages/inportfolio/inportfolio.component';
import { BhavcopyComponent } from './pages/bhavcopy/bhavcopy.component';
import { LotsizeComponent } from './pages/lotsize/lotsize.component';
import { SettlementComponent } from './pages/settlement/settlement.component';
import { HoildaysComponent } from './pages/hoildays/hoildays.component';
import { StockScoresComponent } from './stock-scores/stock-scores.component';
import {OptionScoresComponent} from './option-scores/option-scores.component';
import { EditorNotesComponent } from './editor-notes/editor-notes.component';
import { AddnotesComponent } from './addnotes/addnotes.component';
import { AuthguardGuard } from './authguard.guard';
import { AddScoreComponent } from './pages/add-score/add-score.component';
import { MarketPulseComponent } from './pages/market-pulse/market-pulse.component';
import { MytableComponent } from './pages/mytable/mytable.component';
import { AddLotsizeComponent } from './pages/add-lotsize/add-lotsize.component';
import { IvrReportComponent } from './pages/ivr-report/ivr-report.component';
import { CallPutValuesComponent } from './pages/call-put-values/call-put-values.component';
import { DynamicOptionsComponent } from './pages/dynamic-options/dynamic-options.component';
import { AddDynamicOptionsComponent } from './pages/add-dynamic-options/add-dynamic-options.component';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { AddStockDetailsComponent } from './add-stock-details/add-stock-details.component';
import { AddSettlementComponent } from './pages/add-settlement/add-settlement.component';
import { MarketinternalComponent } from './pages/marketinternal/marketinternal.component';
import { AddMarketinternalComponent } from './pages/add-marketinternal/add-marketinternal.component';
import { MarketinternalCatComponent } from './pages/marketinternal-cat/marketinternal-cat.component';
import { MarketinternalSubcatComponent } from './pages/marketinternal-subcat/marketinternal-subcat.component';
import { OiScoreComponent } from './oi-score/oi-score.component';
import { DataCurrentBhavcopyComponent } from './pages/data-current-bhavcopy/data-current-bhavcopy.component';
import { DataMarketPulseComponent } from './pages/data-market-pulse/data-market-pulse.component';
import { AddLotChangeComponent } from './pages/add-lot-change/add-lot-change.component';
import { IndustryStockComponent } from './pages/industry-stock/industry-stock.component';
import { AddIndustryStockComponent } from './pages/add-industry-stock/add-industry-stock.component';
import { AddNifty50StockComponent } from './pages/add-nifty50-stock/add-nifty50-stock.component';
import { DataMasterComponent } from './pages/data-master/data-master.component';
import { DataMarketInternalsComponent } from './pages/data-market-internals/data-market-internals.component';
import { TradeComponent } from './pages/trade/trade.component';
import { AddTradeComponent } from './pages/add-trade/add-trade.component';
import { AddIndustryComponent } from './pages/add-industry/add-industry.component';
import { JobsComponent } from './pages/jobs/jobs.component';
import { UploadsComponent } from './pages/uploads/uploads.component';
import { AddScoreDetailsComponent } from './pages/add-score-details/add-score-details.component';


export const routes: Routes = [
    { path: '', loadChildren: './pages/login/login.module#LoginModule' },
    { 
        path: '', 
        component: PagesComponent, children: [
            { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardModule', data: { breadcrumb: 'Dashboard' } },
            { path: 'users', loadChildren: './pages/users/users.module#UsersModule', data: { breadcrumb: 'Users' } },
            { path: 'ui', loadChildren: './pages/ui/ui.module#UiModule', data: { breadcrumb: 'UI' } },
            { path: 'form-controls', loadChildren: './pages/form-controls/form-controls.module#FormControlsModule', data: { breadcrumb: 'Form Controls' } },

            { path: 'portfolio/:id', component: PortfolioComponent },
            {path:'addeditors',component:AddnotesComponent},
            {path:'edit-editors/:finsparc_editors_notes_id',component:AddnotesComponent},

            { path:'oi-score',component:OiScoreComponent},

            {path:'editornotes',component:EditorNotesComponent},
            { path: 'inportfolio/:id/:portfolio_id', component:InportfolioComponent},
            { path: 'bhavcopy', component:BhavcopyComponent},
            { path:'uploads' , component:UploadsComponent},
            { path: 'lotsize', component:LotsizeComponent},
            { path: 'settlement', component: SettlementComponent},
            { path:'marketinternal',component:MarketinternalComponent},
            { path:'add-marketinternal',component:AddMarketinternalComponent},   
            { path:'add-marketinternal/:cat_id',component:AddMarketinternalComponent},   
            { path:'add-marketinternal/:cat_id/:subcat_id',component:AddMarketinternalComponent}, 

            { path:'edit-marketinternal/:id/:upkey',component:AddMarketinternalComponent},
            { path:'edit-marketinternal/:id/:cat_id/:upkey',component:AddMarketinternalComponent},
            { path:'edit-marketinternal/:id/:cat_id/:subcat_id/:upkey',component:AddMarketinternalComponent},         
            // { path:'edit-marketinternal/:id?/:cat_id?/:subcat_id?/',component:AddMarketinternalComponent},
            { path:'marketinternal-cat',component:MarketinternalCatComponent},
            { path:'marketinternal-cat/:id',component:MarketinternalCatComponent},
            { path:'marketinternal-subcat',component:MarketinternalSubcatComponent},
            // { path:'marketinternal-subcat/:cat_id/:subcat_id',component:MarketinternalSubcatComponent},
            { path:'marketinternal-subcat/:cat_id/:id',component:MarketinternalSubcatComponent},

            { path: 'holiday', component: HoildaysComponent },
            {path:'stock-scores',component:StockScoresComponent},
            { path:'add-score',component:AddScoreComponent},
            { path:'edit-score/:finsparc_score_id',component:AddScoreComponent},
            
            { path:'mytable',component:MytableComponent},
            { path:'add-lotsize',component:AddLotsizeComponent},
            { path:'add-settlement',component:AddSettlementComponent},
            { path:'edit-settlement/:finsparc_settlementdates_id',component:AddSettlementComponent},
            { path:'edit-lotsize/:finsparc_lotsize_id',component:AddLotsizeComponent},
            { path:'ivr-report',component:IvrReportComponent},
            { path:'call-put-values',component:CallPutValuesComponent},
            { path:'dynamic-option',component:DynamicOptionsComponent},
            { path:'edit-dynamic-option/:id',component:AddDynamicOptionsComponent},
            { path:'add-dynamic-options',component:AddDynamicOptionsComponent},
            { path:'stock-details',component:StockDetailsComponent},
            { path:'add-stock-details',component:AddStockDetailsComponent},
            { path:'edit-stock-details/:id',component:AddStockDetailsComponent},
            { path:'data-current-bhavcopy',component:DataCurrentBhavcopyComponent},
            { path:'data-market-pulse',component:DataMarketPulseComponent },
            { path:'data-master',component:DataMasterComponent },
            { path:'data-market-internals',component:DataMarketInternalsComponent},
            // {path:'option-scores',component:OptionScoresComponent},
            { path:'market-pulse',loadChildren:'./pages/market-pulse/market-pulse.module#MarketPulseModule',data:{ breadcrumb:'Market Pulse' }},
// ****************************************************************
             { path:'lot-change', loadChildren:'./pages/lot-change/lot-change.module#LotChangeModule',data:{breadcrumb:'Lot Change'}},   
             { path:'add-lot-change',component:AddLotChangeComponent},
             { path:'edit-lot-change/:id',component:AddLotChangeComponent},

             { path:'score-details', loadChildren:'./pages/score-details/score-details.module#ScoreDetailsModule',data:{breadcrumb:'Score Details'}},   
             { path:'edit-score-details/:id',component:AddScoreDetailsComponent},
             { path:'industry',loadChildren:'./pages/industry/industry.module#IndustryModule',data:{breadcrumb:'Industry'}},
             
             { path:'industry-stock/:id/:parent_id',component:IndustryStockComponent},
             { path:'add-industry-stock/:id/:parent_id',component:AddIndustryStockComponent},
             { path:'edit-industry-stock/:id/:parent_id',component:AddIndustryStockComponent},

             { path:'add-industry/:id',component:AddIndustryComponent},
             { path:'edit-industry/:id',component:AddIndustryComponent},

             { path:'nifty50-stock',loadChildren:'./pages/nifty50-stock/nifty50-stock.module#Nifty50StockModule',data:{breadcrumb:'Nifty50 Stock'}},

             { path:'add-nifty50_stock/:id/:parent_id',component:AddNifty50StockComponent},
             { path:'edit-nifty50_stock/:id/:parent_id',component:AddNifty50StockComponent},
             {path:'jobs',component:JobsComponent},

            //  { path:'trade',component:TradeComponent },
            { path:'trade',loadChildren:'./pages/trade/trade.module#TradeModule',data:{breadcrumb:'Trade'}},

             { path:'add-trade',component:AddTradeComponent},

             { path:'downloads',loadChildren:'./pages/downloads/downloads.module#DownloadsModule',data:{breadcrumb:'Downloads'}},

            
           
// ****************************************************************
            { path: 'finsparcscore', loadChildren: './pages/finsparc-score/finsparc-score.module#FinsparcScoreModule', data: { breadcrumb: 'Finsparc Score' } },
            { path: 'tables', loadChildren: './pages/tables/tables.module#TablesModule', data: { breadcrumb: 'Tables' } },
            { path: 'icons', loadChildren: './pages/icons/icons.module#IconsModule', data: { breadcrumb: 'Material Icons' } },
            { path: 'drag-drop', loadChildren: './pages/drag-drop/drag-drop.module#DragDropModule', data: { breadcrumb: 'Drag & Drop' } },
            { path: 'schedule', loadChildren: './pages/schedule/schedule.module#ScheduleModule', data: { breadcrumb: 'Schedule' } },
            { path: 'mailbox', loadChildren: './pages/mailbox/mailbox.module#MailboxModule', data: { breadcrumb: 'Mailbox' } },
            { path: 'chat', loadChildren: './pages/chat/chat.module#ChatModule', data: { breadcrumb: 'Chat' } },
            { path: 'maps', loadChildren: './pages/maps/maps.module#MapsModule', data: { breadcrumb: 'Maps' } },
            { path: 'charts', loadChildren: './pages/charts/charts.module#ChartsModule', data: { breadcrumb: 'Charts' } },
            { path: 'dynamic-menu', loadChildren: './pages/dynamic-menu/dynamic-menu.module#DynamicMenuModule', data: { breadcrumb: 'Dynamic Menu' }  },          
            { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' } },
            { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } }
        ],canActivate : [AuthguardGuard]
    },
    { path: 'landing', loadChildren: './pages/landing/landing.module#LandingModule' ,canActivate : [AuthguardGuard]},
    // { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterModule',canActivate:[AuthguardGuard] },
    { path: 'error', component: ErrorComponent, data: { breadcrumb: 'Error' } ,canActivate:[AuthguardGuard]},
    { path: '**', component: NotFoundComponent ,canActivate:[AuthguardGuard]}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
   preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
   useHash: true
});